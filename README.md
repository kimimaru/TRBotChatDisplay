# <img src="./Logo/TRBotChatDisplayLogo.png" alt="TRBot Chat Display Logo" height="43" width="42" align="top"/> TRBotChatDisplay

Custom chat display for [TRBot](https://codeberg.org/kimimaru/TRBot).

![Image of a chat example](./ExampleImages/ChatExample.png)

TRBotChatDisplay reads incoming chat and input messages from TRBot and displays them on a separate window. This window can be added as a source for streaming software, such as OBS, to show the chat on a stream.

## Features

- Displays chat messages from TRBot, showing which services the messages were sent from (Ex. Terminal, Twitch, IRC)
- Highlights messages that are valid inputs in a different text color
- Highly configurable - change font, colors, spacing, and much more
- Selectively show/hide messages from specific services
- Send mock messages to the chat
- Simple configuration file that can be saved and loaded

## Setup

1. TRBot needs to be running with the event dispatcher's WebSocket features enabled. Follow the steps at the top of this guide for how to do this: https://codeberg.org/kimimaru/TRBot/src/branch/master/Wiki/Event-Dispatcher.md
2. In TRBotChatDisplay, enter the WebSocket URI that TRBot's event dispatcher is running under and click "Connect".
3. If the button says "Disconnect", then it's connected to the WebSocket. If not, make sure the WebSocket URI is correct.
4. While connected, you should see chat messages read from TRBot be displayed in the chat window.

## Building

### Prerequisites
- [Qt](https://www.qt.io/) v5.15.8+ for Qt5 or 6.5 for Qt6 - both are supported
- Qt WebSockets
- `qmake`
- `gcc`, `clang`, or another C++ compiler supporting C++11

**Arch Linux**

Qt5
```
sudo pacman -Sy gcc qt5-base qt5-websockets
```

Qt6
```
sudo pacman -Sy gcc qt6-base qt6-websockets
```

**Other Platforms**

Look for Qt5 and Qt6 libraries in your distribution's repositories. Alternatively, install the Qt version you wish to install through either the [online installer](https://www.qt.io/download-qt-installer) or the [offline installer](https://www.qt.io/offline-installers).

### Instructions

1. Clone the repo with `git clone https://codeberg.org/kimimaru/TRBotChatDisplay.git`.
2. `cd TRBotChatDisplay/TRBotChatDisplay`
3. `qmake TRBotChatDisplay.pro` or for Qt6, `qmake6 TRBotChatDisplay.pro`
4. `make`

The executable will be output in the same folder.

## Contributing

The main repository is on Codeberg: https://codeberg.org/kimimaru/TRBotChatDisplay.git

Issues and pull requests are encouraged! Please file an issue if you encounter bugs or have a feature request.

## License
Copyright © 2023 Thomas "Kimimaru" Deeb

[![AGPL](https://www.gnu.org/graphics/agplv3-155x51.png)](https://www.gnu.org/licenses/agpl-3.0.en.html)

TRBotChatDisplay is free software; you are free to run, study, modify, and redistribute it. Specifically, you can modify and/or redistribute TRBotChatDisplay under the terms of the GNU Affero General Public License v3.0 or, at your option, a later version.

See the [logo license](./Logo/Logo%20License) file for the license of TRBotChatDisplay's logo.

<a href="https://endsoftwarepatents.org/innovating-without-patents"><img src="https://static.fsf.org/nosvn/esp/logos/innovating-without-patents.svg" width="120" height="120"></a>
