/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WEBSOCKETCONNECTIONVIEWMODEL_H
#define WEBSOCKETCONNECTIONVIEWMODEL_H

#include <PreferencesViewModel.h>
#include <QtWebSockets/QWebSocket>

class WebSocketConnectionViewModel : public QObject
{
    Q_OBJECT

public:
    WebSocketConnectionViewModel(std::shared_ptr<PreferencesViewModel> prefsVM);

    QString GetCurrentWebSocketURI();
    void ToggleWebSocketConnection();
    bool CanWebSocketBeClosed();

public slots:
    void SetCurrentWebSocketURI(const QString& newURI);

signals:
    void stateChanged(QAbstractSocket::SocketState state);
    void textMessageReceived(const QString &message);
    void WebSocketURILoaded(const QString& newURI);

private:
    std::shared_ptr<PreferencesViewModel> PrefsVM;
    QWebSocket WebSocket;
    QString CurrentWebSocketURI;

    void OnWebSocketConnected();
    void OnWebSocketDisconnected();
    void OnWebSocketTextMessageReceived(const QString& message);
    void OnWebSocketStateChanged(QAbstractSocket::SocketState state);

    void HandlePreferencesUpdated(PreferenceUpdatedEvent updateEvt, PreferencesData newPrefs);
};

#endif // WEBSOCKETCONNECTIONVIEWMODEL_H
