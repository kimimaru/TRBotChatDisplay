/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QObject>

class Constants : public QObject
{
    Q_OBJECT

public:
    enum ClientServiceNames
    {
        terminal,
        twitch,
        websocket,
        irc,
        xmpp,
        matrix
    };
    Q_ENUM(ClientServiceNames)

    static const QSize DefaultServiceImageSize;
    static const QString MessageEventName;
    static const QString InputEventName;
    static const QString BotMessageEventName;

    static QString GetClientServiceName(ClientServiceNames clientServiceNames);
};

#endif // CONSTANTS_H
