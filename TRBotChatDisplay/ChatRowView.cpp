/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QVBoxLayout>
#include <QTimer>
#include <QMetaEnum>
#include <ChatRowView.h>

ChatRowView::ChatRowView(std::shared_ptr<ChatRowViewModel> chatRowVM, QWidget* parent)
    : QWidget(parent)
{
    ChatRowVM = chatRowVM;

    InitUI();
}

ChatRowView::~ChatRowView() noexcept
{

}

void ChatRowView::InitUI()
{
    auto prefsVM = ChatRowVM->GetPrefsVM();

    QFont font = prefsVM->GetChatFont();

    QVBoxLayout* encompassingLayout = new QVBoxLayout(this);

    QHBoxLayout* horizontalLayout = new QHBoxLayout();
    horizontalLayout->setAlignment(Qt::AlignmentFlag::AlignTop | Qt::AlignmentFlag::AlignLeft);

    QVBoxLayout* usernameMsgLayout = new QVBoxLayout();
    usernameMsgLayout->setAlignment(Qt::AlignmentFlag::AlignTop | Qt::AlignmentFlag::AlignLeft);

    QPixmap serviceImg(GetImageNameForService(ChatRowVM->ServiceName));
    serviceImg = serviceImg.scaled(prefsVM->GetServiceIconSize(), Qt::AspectRatioMode::KeepAspectRatio);

    serviceLabel = new QLabel();
    serviceLabel->setFont(font);
    serviceLabel->setAlignment(Qt::AlignmentFlag::AlignTop | Qt::AlignmentFlag::AlignLeft);
    serviceLabel->setPixmap(serviceImg);
    serviceLabel->setVisible(prefsVM->GetShowServiceIcons());

    userNameLabel = new QLabel(ChatRowVM->Username);
    userNameLabel->setObjectName("usernameLabel");
    userNameLabel->setFont(font);
    userNameLabel->setAlignment(Qt::AlignmentFlag::AlignTop | Qt::AlignmentFlag::AlignLeft);
    userNameLabel->setWordWrap(true);
    userNameLabel->setStyleSheet("QLabel#" + userNameLabel->objectName() + " { color : " + prefsVM->GetUsernameTextColor().name(QColor::NameFormat::HexRgb) + "; }");

    messageLabel = new QTextEdit();
    messageLabel->setObjectName("messageLabel");
    messageLabel->setFont(font);
    messageLabel->setReadOnly(true);
    auto msgLabelSizePolicy = messageLabel->sizePolicy();
    msgLabelSizePolicy.setVerticalPolicy(QSizePolicy::Policy::Fixed);
    messageLabel->setSizePolicy(msgLabelSizePolicy);
    messageLabel->setAlignment(Qt::AlignmentFlag::AlignTop | Qt::AlignmentFlag::AlignLeft);
    messageLabel->setWordWrapMode(QTextOption::WrapMode::WrapAtWordBoundaryOrAnywhere);
    messageLabel->setFrameShape(QFrame::NoFrame);
    messageLabel->setTextInteractionFlags(Qt::TextInteractionFlag::NoTextInteraction);
    messageLabel->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    messageLabel->setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    messageLabel->setPlainText(ChatRowVM->MessageText);

    separatorLabel = new QLabel();
    separatorLabel->setObjectName("separatorLabel");
    separatorLabel->setStyleSheet("QLabel#" + separatorLabel->objectName() + " { color : #708090 ; }");
    separatorLabel->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Fixed);
    separatorLabel->setFixedHeight(2);
    separatorLabel->setFrameShape(QFrame::HLine);
    separatorLabel->setVisible(ChatRowVM->GetSeparatorVisible());

    auto messageColorStr = ChatRowVM->GetIsInputMessage()
        ? prefsVM->GetInputTextColor().name(QColor::NameFormat::HexRgb)
        : prefsVM->GetMessageTextColor().name(QColor::NameFormat::HexRgb);

    if (ChatRowVM->GetIsBotMessage() == true)
    {
        userNameLabel->setText(userNameLabel->text() + " [BOT]");
    }

    usernameMsgLayout->addWidget(userNameLabel);
    usernameMsgLayout->addWidget(messageLabel);

    horizontalLayout->addWidget(serviceLabel);
    horizontalLayout->addLayout(usernameMsgLayout);

    encompassingLayout->addLayout(horizontalLayout);
    encompassingLayout->addWidget(separatorLabel);

    connect(prefsVM.get(), &PreferencesViewModel::onPreferencesUpdated, this, &ChatRowView::HandlePreferencesUpdated);
    connect(ChatRowVM.get(), &ChatRowViewModel::isInputMessageChanged, this, &ChatRowView::HandleIsInputMessageChanged);
    connect(ChatRowVM.get(), &ChatRowViewModel::isSeparatorVisibleChanged, separatorLabel, &QLabel::setVisible);

    UpdateMessageStyle();
    UpdateMessageHeight();
    UpdateVisibility();

    //NOTE: Workaround - figure out how to update the height immediately **AFTER** the widget has been put in the layout and painted for the first time
    //Doing this disables the scroll by setting it to the exact height it needs
    QTimer::singleShot(10, this, &ChatRowView::UpdateMessageHeight);
}

QString ChatRowView::GetImageNameForService(const QString& serviceName)
{
    return ":/Images/" + serviceName + "_icon.png";
}

void ChatRowView::HandlePreferencesUpdated(PreferenceUpdatedEvent updateEvt, PreferencesData newPrefs)
{
    auto prefsVM = ChatRowVM->GetPrefsVM();

    auto messageColorStr = ChatRowVM->GetIsInputMessage()
        ? prefsVM->GetInputTextColor().name(QColor::NameFormat::HexRgb)
        : prefsVM->GetMessageTextColor().name(QColor::NameFormat::HexRgb);

    switch (updateEvt)
    {
        case PreferenceUpdatedEvent::UsernameColor:
            userNameLabel->setStyleSheet("QLabel#" + userNameLabel->objectName() + " { color : " + prefsVM->GetUsernameTextColor().name(QColor::NameFormat::HexRgb) + "; }");
            break;
        case PreferenceUpdatedEvent::InputColor:
        case PreferenceUpdatedEvent::MessageColor:
            UpdateMessageStyle();
            break;
        case PreferenceUpdatedEvent::ShowServiceIcons:
            serviceLabel->setVisible(prefsVM->GetShowServiceIcons());
            break;
        case PreferenceUpdatedEvent::ChatFont:
        {
            QFont font = prefsVM->GetChatFont();

            serviceLabel->setFont(font);
            userNameLabel->setFont(font);
            messageLabel->setFont(font);

            UpdateMessageStyle();
            UpdateMessageHeight();

            break;
        }
        case PreferenceUpdatedEvent::ServiceIconSize:
        {
            QPixmap serviceImg(GetImageNameForService(ChatRowVM->ServiceName));
            serviceImg = serviceImg.scaled(prefsVM->GetServiceIconSize(), Qt::AspectRatioMode::KeepAspectRatio);
            serviceLabel->setPixmap(serviceImg);
            break;
        }
        case PreferenceUpdatedEvent::ChatBoxBGColor:
            UpdateMessageStyle();
            break;
        case PreferenceUpdatedEvent::ChatBoxSize:
            UpdateMessageHeight();
            break;
        case PreferenceUpdatedEvent::ShowBotMsgs:
        case PreferenceUpdatedEvent::ShowTerminalMsgs:
        case PreferenceUpdatedEvent::ShowTwitchMsgs:
        case PreferenceUpdatedEvent::ShowWebSocketMsgs:
        case PreferenceUpdatedEvent::ShowIRCMsgs:
        case PreferenceUpdatedEvent::ShowXMPPMsgs:
        case PreferenceUpdatedEvent::ShowMatrixMsgs:
            UpdateVisibility();
            break;
        default:
            break;
    }
}

void ChatRowView::HandleIsInputMessageChanged(bool value)
{
    Q_UNUSED(value);

    UpdateMessageStyle();
}

void ChatRowView::UpdateMessageStyle()
{
    auto prefsVM = ChatRowVM->GetPrefsVM();

    auto messageColorStr = ChatRowVM->GetIsInputMessage()
        ? prefsVM->GetInputTextColor().name(QColor::NameFormat::HexRgb)
        : prefsVM->GetMessageTextColor().name(QColor::NameFormat::HexRgb);

    auto bgColorStr = prefsVM->GetChatBoxBackgroundColor().name(QColor::NameFormat::HexRgb);

    messageLabel->setStyleSheet("QTextEdit#" + messageLabel->objectName() + " { color : " + messageColorStr + "; background-color : " + bgColorStr + "; }");
}

void ChatRowView::UpdateMessageHeight()
{
    messageLabel->setFixedHeight(messageLabel->document()->size().height());
}

void ChatRowView::UpdateVisibility()
{
    auto prefsVM = ChatRowVM->GetPrefsVM();

    auto metaEnum = QMetaEnum::fromType<Constants::ClientServiceNames>();
    auto serviceVal = metaEnum.keyToValue(ChatRowVM->ServiceName.toUtf8().constData());

    bool shouldShow = false;

    switch (serviceVal)
    {
        case Constants::ClientServiceNames::terminal:
            shouldShow = prefsVM->GetShowTerminalMessages();
            break;
        case Constants::ClientServiceNames::twitch:
            shouldShow = prefsVM->GetShowTwitchMessages();
            break;
        case Constants::ClientServiceNames::websocket:
            shouldShow = prefsVM->GetShowWebSocketMessages();
            break;
        case Constants::ClientServiceNames::irc:
            shouldShow = prefsVM->GetShowIRCMessages();
            break;
        case Constants::ClientServiceNames::xmpp:
            shouldShow = prefsVM->GetShowXMPPMessages();
            break;
        case Constants::ClientServiceNames::matrix:
            shouldShow = prefsVM->GetShowMatrixMessages();
        default:
            break;
    }

    if (ChatRowVM->GetIsBotMessage() == true)
    {
        shouldShow &= prefsVM->GetShowBotMessages();
    }

    this->setVisible(shouldShow);
}
