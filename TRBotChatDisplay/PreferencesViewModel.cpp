/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <PreferencesViewModel.h>
#include <QJsonObject>
#include <QJsonDocument>
#include <QStringList>

PreferencesViewModel::PreferencesViewModel()
{

}

bool PreferencesViewModel::GetConnectWebSocketOnLoad() const
{
    return preferencesData.ConnectWebSocketOnLoadPrefs;
}

void PreferencesViewModel::SetConnectWebSocketOnLoad(bool connectWSOnLoad)
{
    if (preferencesData.ConnectWebSocketOnLoadPrefs != connectWSOnLoad)
    {
        preferencesData.ConnectWebSocketOnLoadPrefs = connectWSOnLoad;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ConnectWSOnLoad, preferencesData);
    }
}

QString PreferencesViewModel::GetWebSocketDefaultURI() const
{
    return preferencesData.DefaultWebSocketURI;
}

void PreferencesViewModel::SetWebSocketDefaultURI(const QString& defaultWSURI)
{
    if (preferencesData.DefaultWebSocketURI != defaultWSURI)
    {
        preferencesData.DefaultWebSocketURI = defaultWSURI;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::DefaultWSURI, preferencesData);
    }
}

QColor PreferencesViewModel::GetUsernameTextColor() const
{
    return preferencesData.UsernameTextColor;
}

void PreferencesViewModel::SetUsernameTextColor(const QColor& textColor)
{
    if (preferencesData.UsernameTextColor != textColor)
    {
        preferencesData.UsernameTextColor = textColor;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::UsernameColor, preferencesData);
    }
}

QColor PreferencesViewModel::GetMessageTextColor() const
{
    return preferencesData.MessageTextColor;
}

void PreferencesViewModel::SetMessageTextColor(const QColor& textColor)
{
    if (preferencesData.MessageTextColor != textColor)
    {
        preferencesData.MessageTextColor = textColor;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::MessageColor, preferencesData);
    }
}

QColor PreferencesViewModel::GetInputTextColor() const
{
    return preferencesData.InputTextColor;
}

void PreferencesViewModel::SetInputTextColor(const QColor& textColor)
{
    if (preferencesData.InputTextColor != textColor)
    {
        preferencesData.InputTextColor = textColor;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::InputColor, preferencesData);
    }
}

QFont PreferencesViewModel::GetChatFont() const
{
    return preferencesData.ChatFont;
}

void PreferencesViewModel::SetChatFont(const QFont& chatFont)
{
    if (preferencesData.ChatFont != chatFont)
    {
        preferencesData.ChatFont = chatFont;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ChatFont, preferencesData);
    }
}

QColor PreferencesViewModel::GetChatBoxBackgroundColor() const
{
    return preferencesData.ChatBoxBackgroundColor;
}

void PreferencesViewModel::SetChatBoxBackgroundColor(const QColor& chatBoxBGColor)
{
    if (preferencesData.ChatBoxBackgroundColor != chatBoxBGColor)
    {
        preferencesData.ChatBoxBackgroundColor = chatBoxBGColor;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ChatBoxBGColor, preferencesData);
    }
}

QSize PreferencesViewModel::GetChatBoxSize() const
{
    return preferencesData.ChatBoxSize;
}

void PreferencesViewModel::SetChatBoxSize(const QSize& chatBoxSize)
{
    if (preferencesData.ChatBoxSize != chatBoxSize)
    {
        preferencesData.ChatBoxSize = chatBoxSize;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ChatBoxSize, preferencesData);
    }
}

void PreferencesViewModel::SetChatBoxWidth(const int& chatBoxWidth)
{
    SetChatBoxSize(QSize(chatBoxWidth, preferencesData.ChatBoxSize.height()));
}

void PreferencesViewModel::SetChatBoxHeight(const int& chatBoxHeight)
{
    SetChatBoxSize(QSize(preferencesData.ChatBoxSize.width(), chatBoxHeight));
}

Qt::ScrollBarPolicy PreferencesViewModel::GetChatBoxScrollPolicy() const
{
    return preferencesData.ChatBoxScrollBarPolicy;
}

void PreferencesViewModel::SetChatBoxScrollBarPolicy(const Qt::ScrollBarPolicy& scrollBarPolicy)
{
    if (preferencesData.ChatBoxScrollBarPolicy != scrollBarPolicy)
    {
        preferencesData.ChatBoxScrollBarPolicy = scrollBarPolicy;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ChatBoxScrollBar, preferencesData);
    }
}

void PreferencesViewModel::SetChatBoxScrollBarPolicy(int scrollBarPolicy)
{
    SetChatBoxScrollBarPolicy(static_cast<Qt::ScrollBarPolicy>(scrollBarPolicy));
}

QSize PreferencesViewModel::GetServiceIconSize() const
{
    return preferencesData.ServiceIconSize;
}

void PreferencesViewModel::SetServiceIconSize(const QSize& serviceIconSize)
{
    if (preferencesData.ServiceIconSize != serviceIconSize)
    {
        preferencesData.ServiceIconSize = serviceIconSize;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ServiceIconSize, preferencesData);
    }
}

void PreferencesViewModel::SetServiceIconSize(const int serviceIconSize)
{
    QSize size(serviceIconSize, serviceIconSize);

    SetServiceIconSize(size);
}

int PreferencesViewModel::GetChatRowSpacing() const
{
    return preferencesData.ChatRowSpacing;
}

void PreferencesViewModel::SetChatRowSpacing(int rowSpacing)
{
    if (preferencesData.ChatRowSpacing != rowSpacing)
    {
        preferencesData.ChatRowSpacing = rowSpacing;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ChatRowSpacing, preferencesData);
    }
}

int PreferencesViewModel::GetMaxChatEntries() const
{
    return preferencesData.MaxChatEntries;
}

void PreferencesViewModel::SetMaxChatEntries(int maxEntries)
{
    if (preferencesData.MaxChatEntries != maxEntries)
    {
        preferencesData.MaxChatEntries = maxEntries;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::MaxChatEntries, preferencesData);
    }
}

bool PreferencesViewModel::GetShowBotMessages() const
{
    return preferencesData.ShowBotMessages;
}

void PreferencesViewModel::SetShowBotMessages(bool showBotMsgs)
{
    if (preferencesData.ShowBotMessages != showBotMsgs)
    {
        preferencesData.ShowBotMessages = showBotMsgs;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ShowBotMsgs, preferencesData);
    }
}

bool PreferencesViewModel::GetShowServiceIcons() const
{
    return preferencesData.ShowServiceIcons;
}

void PreferencesViewModel::SetShowServiceIcons(bool showIcons)
{
    if (preferencesData.ShowServiceIcons != showIcons)
    {
        preferencesData.ShowServiceIcons = showIcons;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ShowServiceIcons, preferencesData);
    }
}

bool PreferencesViewModel::GetShowTerminalMessages() const
{
    return preferencesData.ShowTerminalMessages;
}

void PreferencesViewModel::SetShowTerminalMessages(bool showMsgs)
{
    if (preferencesData.ShowTerminalMessages != showMsgs)
    {
        preferencesData.ShowTerminalMessages = showMsgs;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ShowTerminalMsgs, preferencesData);
    }
}

bool PreferencesViewModel::GetShowTwitchMessages() const
{
    return preferencesData.ShowTwitchMessages;
}

void PreferencesViewModel::SetShowTwitchMessages(bool showMsgs)
{
    if (preferencesData.ShowTwitchMessages != showMsgs)
    {
        preferencesData.ShowTwitchMessages = showMsgs;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ShowTwitchMsgs, preferencesData);
    }
}

bool PreferencesViewModel::GetShowWebSocketMessages() const
{
    return preferencesData.ShowWebSocketMessages;
}

void PreferencesViewModel::SetShowWebSocketMessages(bool showMsgs)
{
    if (preferencesData.ShowWebSocketMessages != showMsgs)
    {
        preferencesData.ShowWebSocketMessages = showMsgs;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ShowWebSocketMsgs, preferencesData);
    }
}

bool PreferencesViewModel::GetShowIRCMessages() const
{
    return preferencesData.ShowIRCMessages;
}

void PreferencesViewModel::SetShowIRCMessages(bool showMsgs)
{
    if (preferencesData.ShowIRCMessages != showMsgs)
    {
        preferencesData.ShowIRCMessages = showMsgs;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ShowIRCMsgs, preferencesData);
    }
}

bool PreferencesViewModel::GetShowXMPPMessages() const
{
    return preferencesData.ShowXMPPMessages;
}

void PreferencesViewModel::SetShowXMPPMessages(bool showMsgs)
{
    if (preferencesData.ShowXMPPMessages != showMsgs)
    {
        preferencesData.ShowXMPPMessages = showMsgs;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ShowXMPPMsgs, preferencesData);
    }
}

bool PreferencesViewModel::GetShowMatrixMessages() const
{
    return preferencesData.ShowMatrixMessages;
}

void PreferencesViewModel::SetShowMatrixMessages(bool showMsgs)
{
    if (preferencesData.ShowMatrixMessages != showMsgs)
    {
        preferencesData.ShowMatrixMessages = showMsgs;

        emit onPreferencesUpdated(PreferenceUpdatedEvent::ShowMatrixMsgs, preferencesData);
    }
}

void PreferencesViewModel::ResetPreferences()
{
    LoadPreferencesFromObject(PreferencesData());
}

QString PreferencesViewModel::SavePreferencesAsString()
{
    QJsonObject jsonObj = QJsonObject();

    QColor::NameFormat colorNameFormat = QColor::NameFormat::HexRgb;

    jsonObj[ConnectWebSocketLoadOnPrefs_Name] = preferencesData.ConnectWebSocketOnLoadPrefs;
    jsonObj[DefaultWebSocketURI_Name] =         preferencesData.DefaultWebSocketURI;
    jsonObj[UsernameTextColor_Name] =           preferencesData.UsernameTextColor.name(colorNameFormat);
    jsonObj[MessageTextColor_Name] =            preferencesData.MessageTextColor.name(colorNameFormat);
    jsonObj[InputTextColor_Name] =              preferencesData.InputTextColor.name(colorNameFormat);
    jsonObj[ChatFontFamilyName_Name] =          preferencesData.ChatFont.family();
    jsonObj[ChatFontSize_Name] =                preferencesData.ChatFont.pointSize();
    jsonObj[ChatBoxBackgroundColor_Name] =      preferencesData.ChatBoxBackgroundColor.name(colorNameFormat);
    jsonObj[ChatBoxSize_Name] =                 ConvertQSizeToQString(preferencesData.ChatBoxSize);
    jsonObj[ChatBoxScrollBarPolicy_Name] =      static_cast<int>(preferencesData.ChatBoxScrollBarPolicy);
    jsonObj[ServiceIconSize_Name] =             ConvertQSizeToQString(preferencesData.ServiceIconSize);
    jsonObj[ChatRowSpacing_Name] =              preferencesData.ChatRowSpacing;
    jsonObj[MaxChatEntries_Name] =              preferencesData.MaxChatEntries;
    jsonObj[ShowBotMessages_Name] =             preferencesData.ShowBotMessages;
    jsonObj[ShowServiceIcons_Name] =            preferencesData.ShowServiceIcons;
    jsonObj[ShowTerminalMessages_Name] =        preferencesData.ShowTerminalMessages;
    jsonObj[ShowTwitchMessages_Name] =          preferencesData.ShowTwitchMessages;
    jsonObj[ShowWebSocketMessages_Name] =       preferencesData.ShowWebSocketMessages;
    jsonObj[ShowIRCMessages_Name] =             preferencesData.ShowIRCMessages;
    jsonObj[ShowXMPPMessages_Name] =            preferencesData.ShowXMPPMessages;
    jsonObj[ShowMatrixMessages_Name] =          preferencesData.ShowMatrixMessages;

    QJsonDocument doc = QJsonDocument();
    doc.setObject(jsonObj);

    return doc.toJson(QJsonDocument::JsonFormat::Indented);
}

bool PreferencesViewModel::ValidatePreferencesJsonFromString(QByteArray preferencesJson)
{
    QJsonDocument doc = QJsonDocument::fromJson(preferencesJson);
    QJsonObject jsonObj = doc.object();

    QStringList allJsonKeys =
    {
        ConnectWebSocketLoadOnPrefs_Name,
        DefaultWebSocketURI_Name,
        UsernameTextColor_Name,
        MessageTextColor_Name,
        InputTextColor_Name,
        ChatFontFamilyName_Name,
        ChatFontSize_Name,
        ChatBoxBackgroundColor_Name,
        ChatBoxSize_Name,
        ChatBoxScrollBarPolicy_Name,
        ServiceIconSize_Name,
        ChatRowSpacing_Name,
        MaxChatEntries_Name,
        ShowBotMessages_Name,
        ShowServiceIcons_Name,
        ShowTerminalMessages_Name,
        ShowTwitchMessages_Name,
        ShowWebSocketMessages_Name,
        ShowIRCMessages_Name,
        ShowXMPPMessages_Name,
        ShowMatrixMessages_Name
    };

    for (int i = 0; i < allJsonKeys.size(); i++)
    {
        if (jsonObj.contains(allJsonKeys[i]) == false)
        {
            return false;
        }
    }

    return true;
}

void PreferencesViewModel::LoadPreferencesFromString(QByteArray preferencesJson)
{
    QJsonDocument doc = QJsonDocument::fromJson(preferencesJson);
    QJsonObject jsonObj = doc.object();

    PreferencesData prefsData = PreferencesData();

    prefsData.ConnectWebSocketOnLoadPrefs =     jsonObj[ConnectWebSocketLoadOnPrefs_Name].toBool();
    prefsData.DefaultWebSocketURI =             jsonObj[DefaultWebSocketURI_Name].toString();
    prefsData.UsernameTextColor =               ConvertQStringToQColor(jsonObj[UsernameTextColor_Name].toString());
    prefsData.MessageTextColor =                ConvertQStringToQColor(jsonObj[MessageTextColor_Name].toString());
    prefsData.InputTextColor =                  ConvertQStringToQColor(jsonObj[InputTextColor_Name].toString());
    prefsData.ChatFont =                        QFont(jsonObj[ChatFontFamilyName_Name].toString(),
                                                      jsonObj[ChatFontSize_Name].toInt());
    prefsData.ChatBoxBackgroundColor =          ConvertQStringToQColor(jsonObj[ChatBoxBackgroundColor_Name].toString());
    prefsData.ChatBoxSize =                     ConvertQStringToQSize(jsonObj[ChatBoxSize_Name].toString());
    prefsData.ChatBoxScrollBarPolicy =          static_cast<Qt::ScrollBarPolicy>(jsonObj[ChatBoxScrollBarPolicy_Name].toInt());
    prefsData.ServiceIconSize =                 ConvertQStringToQSize(jsonObj[ServiceIconSize_Name].toString());
    prefsData.ChatRowSpacing =                  jsonObj[ChatRowSpacing_Name].toInt();
    prefsData.MaxChatEntries =                  jsonObj[MaxChatEntries_Name].toInt();
    prefsData.ShowBotMessages =                 jsonObj[ShowBotMessages_Name].toBool();
    prefsData.ShowServiceIcons =                jsonObj[ShowServiceIcons_Name].toBool();
    prefsData.ShowTerminalMessages =            jsonObj[ShowTerminalMessages_Name].toBool();
    prefsData.ShowTwitchMessages =              jsonObj[ShowTwitchMessages_Name].toBool();
    prefsData.ShowWebSocketMessages =           jsonObj[ShowWebSocketMessages_Name].toBool();
    prefsData.ShowIRCMessages =                 jsonObj[ShowIRCMessages_Name].toBool();
    prefsData.ShowXMPPMessages =                jsonObj[ShowXMPPMessages_Name].toBool();
    prefsData.ShowMatrixMessages =              jsonObj[ShowMatrixMessages_Name].toBool();

    LoadPreferencesFromObject(prefsData);
}

void PreferencesViewModel::LoadPreferencesFromObject(PreferencesData newPrefs)
{
    SetConnectWebSocketOnLoad(newPrefs.ConnectWebSocketOnLoadPrefs);
    SetWebSocketDefaultURI(newPrefs.DefaultWebSocketURI);
    SetUsernameTextColor(newPrefs.UsernameTextColor);
    SetMessageTextColor(newPrefs.MessageTextColor);
    SetInputTextColor(newPrefs.InputTextColor);
    SetChatFont(newPrefs.ChatFont);
    SetChatBoxBackgroundColor(newPrefs.ChatBoxBackgroundColor);
    SetChatBoxSize(newPrefs.ChatBoxSize);
    SetChatBoxScrollBarPolicy(newPrefs.ChatBoxScrollBarPolicy);
    SetServiceIconSize(newPrefs.ServiceIconSize);
    SetChatRowSpacing(newPrefs.ChatRowSpacing);
    SetMaxChatEntries(newPrefs.MaxChatEntries);
    SetShowBotMessages(newPrefs.ShowBotMessages);
    SetShowServiceIcons(newPrefs.ShowServiceIcons);
    SetShowTerminalMessages(newPrefs.ShowTerminalMessages);
    SetShowTwitchMessages(newPrefs.ShowTwitchMessages);
    SetShowWebSocketMessages(newPrefs.ShowWebSocketMessages);
    SetShowIRCMessages(newPrefs.ShowIRCMessages);
    SetShowXMPPMessages(newPrefs.ShowXMPPMessages);
    SetShowMatrixMessages(newPrefs.ShowMatrixMessages);

    emit onPreferencesUpdated(PreferenceUpdatedEvent::LoadedFromFile, newPrefs);
}

QString PreferencesViewModel::ConvertQSizeToQString(const QSize& size)
{
    return QString::number(size.width()) + "," + QString::number(size.height());
}

QSize PreferencesViewModel::ConvertQStringToQSize(const QString& sizeStr)
{
    QStringList list = sizeStr.split(',');
    return QSize(list[0].toInt(), list[1].toInt());
}

QColor PreferencesViewModel::ConvertQStringToQColor(const QString& colorStr)
{
    QStringList list = colorStr.split('#');

    int stringAsInt = list[1].toInt(nullptr, 16);
    QColor color = QColor::fromRgb(stringAsInt);

    return color;
}
