/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHATROWVIEW_H
#define CHATROWVIEW_H

#include <QImageReader>
#include <QLabel>
#include <QTextEdit>
#include <ChatRowViewModel.h>
#include <PreferencesViewModel.h>

class ChatRowView : public QWidget
{
    Q_OBJECT

public:

    ChatRowView(std::shared_ptr<ChatRowViewModel> chatRowVM, QWidget* parent = nullptr);
    ~ChatRowView() noexcept;

private:
    std::shared_ptr<ChatRowViewModel> ChatRowVM;
    QLabel* serviceLabel;
    QLabel* userNameLabel;
    QTextEdit* messageLabel;
    QLabel* separatorLabel;

    void InitUI();
    QString GetImageNameForService(const QString& serviceName);
    void HandlePreferencesUpdated(PreferenceUpdatedEvent updateEvt, PreferencesData newPrefs);
    void HandleIsInputMessageChanged(bool value);
    void UpdateMessageStyle();
    void UpdateMessageHeight();
    void UpdateVisibility();
};

#endif // CHATROWVIEW_H
