/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include <MainWindow.h>
#include <MainWindowViewModel.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setApplicationName("TRBot Chat Display");
    a.setApplicationDisplayName("TRBot Chat Display");
    a.setWindowIcon(QIcon(":/Images/Logo/TRBotChatDisplayLogo.png"));
    a.setOrganizationName("Thomas \"Kimimaru\" Deeb");
    a.setOrganizationDomain("https://codeberg.org/kimimaru/TRBotChatDisplay");

    std::shared_ptr<PreferencesViewModel> prefsVM = std::shared_ptr<PreferencesViewModel>(new PreferencesViewModel());
    std::shared_ptr<WebSocketConnectionViewModel> webSocketVM = std::shared_ptr<WebSocketConnectionViewModel>(new WebSocketConnectionViewModel(prefsVM));
    std::shared_ptr<ChatDisplayViewModel> chatVM = std::shared_ptr<ChatDisplayViewModel>(new ChatDisplayViewModel(prefsVM, webSocketVM, QSize(400, 150)));

    std::shared_ptr<MainWindowViewModel> mainWindowVM = std::shared_ptr<MainWindowViewModel>(new MainWindowViewModel(prefsVM, chatVM, webSocketVM, QSize(450, 150)));

    MainWindow w(mainWindowVM);
    w.show();
    int exec = a.exec();

    return exec;
}
