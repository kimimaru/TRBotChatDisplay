/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QHBoxLayout>
#include <QLabel>
#include <WebSocketConnectionView.h>

WebSocketConnectionView::WebSocketConnectionView(std::shared_ptr<WebSocketConnectionViewModel> webSocketVM)
{
    WebSocketVM = webSocketVM;

    InitUI();
}

void WebSocketConnectionView::InitUI()
{
    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setSizeConstraint(QLayout::SizeConstraint::SetMaximumSize);

    QLabel* wsURILabel = new QLabel(tr("WebSocket URI"));
    DefaultWSURIBox = new QLineEdit(WebSocketVM->GetCurrentWebSocketURI());
    WSButton = new QPushButton();

    layout->addWidget(wsURILabel);
    layout->addWidget(DefaultWSURIBox);
    layout->addWidget(WSButton);
    layout->setSpacing(10);

    HandleWebSocketStateChange(QAbstractSocket::SocketState::UnconnectedState);

    connect(WebSocketVM.get(), &WebSocketConnectionViewModel::stateChanged, this, &WebSocketConnectionView::HandleWebSocketStateChange);
    connect(WSButton, &QPushButton::pressed, WebSocketVM.get(), &WebSocketConnectionViewModel::ToggleWebSocketConnection);
    connect(DefaultWSURIBox, &QLineEdit::textChanged, WebSocketVM.get(), &WebSocketConnectionViewModel::SetCurrentWebSocketURI);
    connect(WebSocketVM.get(), &WebSocketConnectionViewModel::WebSocketURILoaded, DefaultWSURIBox, &QLineEdit::setText);
}

void WebSocketConnectionView::HandleWebSocketStateChange(QAbstractSocket::SocketState state)
{
    switch (state)
    {
        case QAbstractSocket::SocketState::ConnectingState:
            WSButton->setText(tr("Connecting..."));
            WSButton->setEnabled(false);
            DefaultWSURIBox->setReadOnly(true);
            break;
        case QAbstractSocket::SocketState::ConnectedState:
            WSButton->setText(tr("Disconnect"));
            WSButton->setEnabled(true);
            DefaultWSURIBox->setReadOnly(true);
            break;
        case QAbstractSocket::SocketState::ClosingState:
            WSButton->setText(tr("Disconnecting..."));
            WSButton->setEnabled(false);
            DefaultWSURIBox->setReadOnly(true);
            break;
        case QAbstractSocket::SocketState::UnconnectedState:
            WSButton->setText(tr("Connect"));
            WSButton->setEnabled(true);
            DefaultWSURIBox->setReadOnly(false);
            break;
        default:
            break;
    }
}
