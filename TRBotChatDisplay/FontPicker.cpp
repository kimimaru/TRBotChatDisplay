/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <FontPicker.h>
#include <iostream>

FontPicker::FontPicker(const QFont& initial, QWidget* parent)
    : QPushButton(parent)
{
    CurrentFont = initial;

    UpdateButtonFont();

    connect(this, &QPushButton::clicked, this, &FontPicker::OnFontButtonPressed);
}

FontPicker::~FontPicker()
{

}

void FontPicker::SetFont(const QFont& font)
{
    if (CurrentFont != font)
    {
        CurrentFont = font;
        UpdateButtonFont();

        emit currentFontChanged(font);
    }
}

void FontPicker::OnFontButtonPressed()
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, CurrentFont, this);

    if (ok == false)
    {
        return;
    }

    SetFont(font);
}

void FontPicker::UpdateButtonFont()
{
    QString labelText = CurrentFont.family() + " " + QString::number(CurrentFont.pointSize()) + "pt";

    this->setText(labelText);
}
