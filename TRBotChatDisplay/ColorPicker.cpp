/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <ColorPicker.h>
#include <iostream>

ColorPicker::ColorPicker(const QColor& initial, QWidget* parent)
    : QPushButton(parent)
{
    CurrentColor = initial;
    this->setObjectName("colorpicker");

    UpdateButtonColor();

    connect(this, &QPushButton::clicked, this, &ColorPicker::OnColorButtonPressed);
}

ColorPicker::~ColorPicker()
{

}

void ColorPicker::SetColor(const QColor& color)
{
    if (CurrentColor != color)
    {
        CurrentColor = color;
        UpdateButtonColor();

        emit currentColorChanged(color);
    }
}

void ColorPicker::OnColorButtonPressed()
{
    QColor color = QColorDialog::getColor(CurrentColor, this);

    if (color.isValid() == false)
    {
        return;
    }

    SetColor(color);
}

void ColorPicker::UpdateButtonColor()
{
    this->setStyleSheet("QPushButton#colorpicker { background-color: " + CurrentColor.name(QColor::NameFormat::HexRgb) + "; }");
}
