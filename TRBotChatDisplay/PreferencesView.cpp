/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QObject>
#include <QWidget>
#include <QCheckBox>
#include <QLineEdit>
#include <QColorDialog>
#include <PreferencesView.h>
#include <ColorPicker.h>

PreferencesView::PreferencesView(std::shared_ptr<PreferencesViewModel> prefsVM, QWidget* parent)
    : QWidget(parent)
{
    PrefsViewModel = prefsVM;

    InitUI();
}

PreferencesView::~PreferencesView() noexcept
{
    
}

void PreferencesView::InitUI()
{
    QVBoxLayout* verticalLayout = new QVBoxLayout(this);
    verticalLayout->setSizeConstraint(QLayout::SizeConstraint::SetMinAndMaxSize);
    verticalLayout->setAlignment(Qt::AlignmentFlag::AlignTop | Qt::AlignmentFlag::AlignCenter);

    QWidget* gridContainer = new QWidget();

    QGridLayout* gridLayout = new QGridLayout(gridContainer);
    gridLayout->setAlignment(Qt::AlignmentFlag::AlignTop | Qt::AlignmentFlag::AlignCenter);
    gridLayout->setSizeConstraint(QLayout::SizeConstraint::SetMaximumSize);

    ConnectWebSocketOnPrefsLoadBox = new QCheckBox(tr("Connect WebSocket when prefs loaded"));
    WebSocketURILineEdit = new QLineEdit(PrefsViewModel->GetWebSocketDefaultURI());

    UserNameTextColorPicker = new ColorPicker(PrefsViewModel->GetUsernameTextColor());
    MessageTextColorPicker = new ColorPicker(PrefsViewModel->GetMessageTextColor());
    InputTextColorPicker = new ColorPicker(PrefsViewModel->GetInputTextColor());
    ChatFontPicker = new FontPicker(PrefsViewModel->GetChatFont());
    ChatBGColorPicker = new ColorPicker(PrefsViewModel->GetChatBoxBackgroundColor());
    ChatBoxWidthSpinBox = new QSpinBox();
    ChatBoxHeightSpinBox = new QSpinBox();
    ChatBoxScrollBarBox = new QComboBox();
    ServiceIconSizeSpinBox = new QSpinBox();
    ChatRowSpacingSpinBox = new QSpinBox();
    MaxChatEntriesSpinBox = new QSpinBox();
    ShowBotMessagesBox = new QCheckBox(tr("Show bot messages"));
    ShowServiceIconBox = new QCheckBox(tr("Show service icons"));
    MessageToggleGroupBox = new QGroupBox(tr("Display Messages From"));

    QHBoxLayout* toggleLayout = new QHBoxLayout();
    toggleLayout->setAlignment(Qt::AlignmentFlag::AlignTop | Qt::AlignmentFlag::AlignLeft);
    TerminalMessageCheckbox = new QCheckBox(tr("Terminal"));
    TwitchMessageCheckbox = new QCheckBox(tr("Twitch"));
    WebSocketMessageCheckbox = new QCheckBox(tr("WebSocket"));
    IRCMessageCheckbox = new QCheckBox(tr("IRC"));
    XMPPMessageCheckbox = new QCheckBox(tr("XMPP"));
    MatrixMessageCheckbox = new QCheckBox(tr("Matrix"));

    ChatBoxScrollBarBox->addItem("Auto");
    ChatBoxScrollBarBox->addItem("Off");
    ChatBoxScrollBarBox->addItem("On");

    TerminalMessageCheckbox->setCheckState(PrefsViewModel->GetShowTerminalMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
    TwitchMessageCheckbox->setCheckState(PrefsViewModel->GetShowTwitchMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
    WebSocketMessageCheckbox->setCheckState(PrefsViewModel->GetShowWebSocketMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
    IRCMessageCheckbox->setCheckState(PrefsViewModel->GetShowIRCMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
    XMPPMessageCheckbox->setCheckState(PrefsViewModel->GetShowXMPPMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
    MatrixMessageCheckbox->setCheckState(PrefsViewModel->GetShowMatrixMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);

    toggleLayout->addWidget(TerminalMessageCheckbox);
    toggleLayout->addWidget(TwitchMessageCheckbox);
    toggleLayout->addWidget(WebSocketMessageCheckbox);
    toggleLayout->addWidget(IRCMessageCheckbox);
    toggleLayout->addWidget(XMPPMessageCheckbox);
    toggleLayout->addWidget(MatrixMessageCheckbox);

    QWidget* chatBoxSizeWidget = new QWidget();
    QHBoxLayout* chatBoxSizeLayout = new QHBoxLayout();
    chatBoxSizeLayout->setSizeConstraint(QLayout::SizeConstraint::SetMinAndMaxSize);

    ChatBoxWidthSpinBox->setMaximum(19200);
    ChatBoxWidthSpinBox->setValue(PrefsViewModel->GetChatBoxSize().width());
    ChatBoxWidthSpinBox->setAccelerated(true);

    ChatBoxHeightSpinBox->setMaximum(19200);
    ChatBoxHeightSpinBox->setValue(PrefsViewModel->GetChatBoxSize().height());
    ChatBoxHeightSpinBox->setAccelerated(true);

    chatBoxSizeLayout->addWidget(ChatBoxWidthSpinBox);
    chatBoxSizeLayout->addWidget(ChatBoxHeightSpinBox);
    chatBoxSizeWidget->setLayout(chatBoxSizeLayout);

    ServiceIconSizeSpinBox->setMinimum(1);
    ServiceIconSizeSpinBox->setMaximum(256);
    ServiceIconSizeSpinBox->setValue(PrefsViewModel->GetServiceIconSize().width());
    ServiceIconSizeSpinBox->setSuffix(" px");
    ServiceIconSizeSpinBox->setAccelerated(true);

    ChatRowSpacingSpinBox->setMinimum(0);
    ChatRowSpacingSpinBox->setMaximum(100);
    ChatRowSpacingSpinBox->setValue(PrefsViewModel->GetChatRowSpacing());
    ChatRowSpacingSpinBox->setSuffix(" px");
    ChatRowSpacingSpinBox->setAccelerated(true);

    MaxChatEntriesSpinBox->setMinimum(1);
    MaxChatEntriesSpinBox->setMaximum(9999);
    MaxChatEntriesSpinBox->setValue(PrefsViewModel->GetMaxChatEntries());
    MaxChatEntriesSpinBox->setAccelerated(true);

    ShowBotMessagesBox->setTristate(false);
    ShowBotMessagesBox->setCheckState(PrefsViewModel->GetShowBotMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);

    ShowServiceIconBox->setTristate(false);
    ShowServiceIconBox->setCheckState(PrefsViewModel->GetShowServiceIcons() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);

    MessageToggleGroupBox->setLayout(toggleLayout);

    QList<QWidget*> leftWidgets =
    {
        new QLabel(tr("Connect to WebSocket on startup")),
        new QLabel(tr("Default WebSocket URI")),
        new QLabel(tr("Username Text Color")),
        new QLabel(tr("Message Text Color")),
        new QLabel(tr("Input Text Color")),
        new QLabel(tr("Chatbox Font")),
        new QLabel(tr("Chatbox Background Color")),
        new QLabel(tr("Chatbox Size")),
        new QLabel(tr("Chatbox Scroll Bar")),
        new QLabel(tr("Chat Service Icon Size")),
        new QLabel(tr("Chat Row Spacing")),
        new QLabel(tr("Max Chat Entries")),
        ShowBotMessagesBox,
        ShowServiceIconBox
    };

    QList<QWidget*> rightWidgets =
    {
        ConnectWebSocketOnPrefsLoadBox,
        WebSocketURILineEdit,
        UserNameTextColorPicker,
        MessageTextColorPicker,
        InputTextColorPicker,
        ChatFontPicker,
        ChatBGColorPicker,
        chatBoxSizeWidget,
        ChatBoxScrollBarBox,
        ServiceIconSizeSpinBox,
        ChatRowSpacingSpinBox,
        MaxChatEntriesSpinBox
    };

    //Add left side widgets
    for (int i = 0; i < leftWidgets.count(); i++)
    {
        gridLayout->addWidget(leftWidgets[i], i, 0);
    }

    //Add right side widgets
    for (int i = 0; i < rightWidgets.count(); i++)
    {
        gridLayout->addWidget(rightWidgets[i], i, 1);
    }

    verticalLayout->addWidget(gridContainer);
    verticalLayout->addWidget(MessageToggleGroupBox);

    connect(PrefsViewModel.get(), &PreferencesViewModel::onPreferencesUpdated, this, &PreferencesView::HandlePreferencesUpdated);

    connect(ConnectWebSocketOnPrefsLoadBox, &QCheckBox::toggled, PrefsViewModel.get(), &PreferencesViewModel::SetConnectWebSocketOnLoad);
    connect(WebSocketURILineEdit, &QLineEdit::textChanged, PrefsViewModel.get(), &PreferencesViewModel::SetWebSocketDefaultURI);
    connect(UserNameTextColorPicker, &ColorPicker::currentColorChanged, PrefsViewModel.get(), &PreferencesViewModel::SetUsernameTextColor);
    connect(MessageTextColorPicker, &ColorPicker::currentColorChanged, PrefsViewModel.get(), &PreferencesViewModel::SetMessageTextColor);
    connect(InputTextColorPicker, &ColorPicker::currentColorChanged, PrefsViewModel.get(), &PreferencesViewModel::SetInputTextColor);
    connect(ChatFontPicker, &FontPicker::currentFontChanged, PrefsViewModel.get(), &PreferencesViewModel::SetChatFont);
    connect(ChatBGColorPicker, &ColorPicker::currentColorChanged, PrefsViewModel.get(), &PreferencesViewModel::SetChatBoxBackgroundColor);
    connect(ChatBoxWidthSpinBox, &QSpinBox::valueChanged, PrefsViewModel.get(), &PreferencesViewModel::SetChatBoxWidth);
    connect(ChatBoxHeightSpinBox, &QSpinBox::valueChanged, PrefsViewModel.get(), &PreferencesViewModel::SetChatBoxHeight);
    connect(ChatBoxScrollBarBox, &QComboBox::currentIndexChanged, PrefsViewModel.get(), QOverload<int>::of(&PreferencesViewModel::SetChatBoxScrollBarPolicy));
    connect(ServiceIconSizeSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), PrefsViewModel.get(), QOverload<int>::of(&PreferencesViewModel::SetServiceIconSize));
    connect(ChatRowSpacingSpinBox, &QSpinBox::valueChanged, PrefsViewModel.get(), &PreferencesViewModel::SetChatRowSpacing);
    connect(MaxChatEntriesSpinBox, &QSpinBox::valueChanged, PrefsViewModel.get(), &PreferencesViewModel::SetMaxChatEntries);
    connect(ShowBotMessagesBox, &QCheckBox::toggled, PrefsViewModel.get(), &PreferencesViewModel::SetShowBotMessages);
    connect(ShowServiceIconBox, &QCheckBox::toggled, PrefsViewModel.get(), &PreferencesViewModel::SetShowServiceIcons);
    connect(TerminalMessageCheckbox, &QCheckBox::toggled, PrefsViewModel.get(), &PreferencesViewModel::SetShowTerminalMessages);
    connect(TwitchMessageCheckbox, &QCheckBox::toggled, PrefsViewModel.get(), &PreferencesViewModel::SetShowTwitchMessages);
    connect(WebSocketMessageCheckbox, &QCheckBox::toggled, PrefsViewModel.get(), &PreferencesViewModel::SetShowWebSocketMessages);
    connect(IRCMessageCheckbox, &QCheckBox::toggled, PrefsViewModel.get(), &PreferencesViewModel::SetShowIRCMessages);
    connect(XMPPMessageCheckbox, &QCheckBox::toggled, PrefsViewModel.get(), &PreferencesViewModel::SetShowXMPPMessages);
    connect(MatrixMessageCheckbox, &QCheckBox::toggled, PrefsViewModel.get(), &PreferencesViewModel::SetShowMatrixMessages);
}

void PreferencesView::HandlePreferencesUpdated(PreferenceUpdatedEvent updateEvt, PreferencesData newPrefs)
{
    switch (updateEvt)
    {
        case ConnectWSOnLoad:
            ConnectWebSocketOnPrefsLoadBox->setCheckState(PrefsViewModel->GetConnectWebSocketOnLoad() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
            break;
        case PreferenceUpdatedEvent::UsernameColor:
            UserNameTextColorPicker->SetColor(PrefsViewModel->GetUsernameTextColor());
            break;
        case PreferenceUpdatedEvent::MessageColor:
            MessageTextColorPicker->SetColor(PrefsViewModel->GetMessageTextColor());
            break;
        case PreferenceUpdatedEvent::InputColor:
            InputTextColorPicker->SetColor(PrefsViewModel->GetInputTextColor());
            break;
        case PreferenceUpdatedEvent::ChatBoxBGColor:
            ChatBGColorPicker->SetColor(PrefsViewModel->GetChatBoxBackgroundColor());
            break;
        case PreferenceUpdatedEvent::ChatBoxSize:
            ChatBoxWidthSpinBox->setValue(PrefsViewModel->GetChatBoxSize().width());
            ChatBoxHeightSpinBox->setValue(PrefsViewModel->GetChatBoxSize().height());
            break;
        case PreferenceUpdatedEvent::ChatBoxScrollBar:
            ChatBoxScrollBarBox->setCurrentIndex(PrefsViewModel->GetChatBoxScrollPolicy());
            break;
        case PreferenceUpdatedEvent::ChatFont:
            ChatFontPicker->SetFont(PrefsViewModel->GetChatFont());
            break;
        case PreferenceUpdatedEvent::ServiceIconSize:
            ServiceIconSizeSpinBox->setValue(PrefsViewModel->GetServiceIconSize().width());
            break;
        case PreferenceUpdatedEvent::ChatRowSpacing:
            ChatRowSpacingSpinBox->setValue(PrefsViewModel->GetChatRowSpacing());
            break;
        case PreferenceUpdatedEvent::MaxChatEntries:
            MaxChatEntriesSpinBox->setValue(PrefsViewModel->GetMaxChatEntries());
            break;
        case PreferenceUpdatedEvent::ShowBotMsgs:
            ShowBotMessagesBox->setCheckState(PrefsViewModel->GetShowBotMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
            break;
        case PreferenceUpdatedEvent::ShowServiceIcons:
            ShowServiceIconBox->setCheckState(PrefsViewModel->GetShowServiceIcons() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
            break;
        case PreferenceUpdatedEvent::ShowTerminalMsgs:
            TerminalMessageCheckbox->setCheckState(PrefsViewModel->GetShowTerminalMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
            break;
        case PreferenceUpdatedEvent::ShowTwitchMsgs:
            TwitchMessageCheckbox->setCheckState(PrefsViewModel->GetShowTwitchMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
            break;
        case PreferenceUpdatedEvent::ShowWebSocketMsgs:
            WebSocketMessageCheckbox->setCheckState(PrefsViewModel->GetShowWebSocketMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
            break;
        case PreferenceUpdatedEvent::ShowIRCMsgs:
            IRCMessageCheckbox->setCheckState(PrefsViewModel->GetShowIRCMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
            break;
        case PreferenceUpdatedEvent::ShowXMPPMsgs:
            XMPPMessageCheckbox->setCheckState(PrefsViewModel->GetShowXMPPMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
            break;
        case PreferenceUpdatedEvent::ShowMatrixMsgs:
            MatrixMessageCheckbox->setCheckState(PrefsViewModel->GetShowMatrixMessages() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
            break;
        case LoadedFromFile:
            WebSocketURILineEdit->setText(PrefsViewModel->GetWebSocketDefaultURI());
            break;
        default:
            break;
    }
}
