/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CUSTOMTEXTFORMVIEWMODEL_H
#define CUSTOMTEXTFORMVIEWMODEL_H

#include <QObject>
#include <QWidget>
#include <QVariant>
#include <QStringLiteral>
#include <QRegularExpression>
#include <QUuid>
#include <ChatDisplayViewModel.h>
#include <ChatRowViewModel.h>
#include <PreferencesViewModel.h>
#include <Constants.h>

class CustomTextFormViewModel : public QObject
{
    Q_OBJECT

public slots:
    void SetSentUsernameText(const QString& value);
    void SetSentMessageText(const QString& value);

    void SetSendMessageAsInput(const bool value);
    void SetSendAsBotMessage(const bool value);
    void SetClearSentMessageDetailsAfterSend(const bool value);
    void SetSentMessageServiceName(const QString& value);

public:
    CustomTextFormViewModel(std::shared_ptr<PreferencesViewModel> prefsVM, std::shared_ptr<ChatDisplayViewModel> chatVM);

    void SendMessageToChat();

signals:
    void SentUsernameTextUpdated(const QString&);
    void SentMessageTextUpdated(const QString&);
    void SentMessageServiceNameUpdated(const QString&);
    bool CanSendMessageUpdated(bool);

private:
    std::shared_ptr<ChatDisplayViewModel> ChatViewModel;
    std::shared_ptr<PreferencesViewModel> PrefsVM;

    QString SentUsernameText = "";
    QString SentMessageText = "";
    QString SentMessageServiceName = QVariant::fromValue(Constants::ClientServiceNames::terminal).toString();
    bool SendMessageAsInput = false;
    bool SendAsBotMessage = false;
    bool ClearSentMessageDetailsAfterSend = false;

    QRegularExpression WhiteSpaceRegex;

    bool IsWhiteSpace(const QString & str) const;

    bool CanSendMessage() const;
};

#endif // CUSTOMTEXTFORMVIEWMODEL_H
