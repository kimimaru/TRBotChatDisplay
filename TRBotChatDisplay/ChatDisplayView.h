/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHATDISPLAYVIEW_H
#define CHATDISPLAYVIEW_H

#include <QObject>
#include <QWidget>
#include <QDialog>
#include <QScrollArea>
#include <QLabel>
#include <QMainWindow>
#include <QVBoxLayout>
#include <ChatDisplayViewModel.h>

/**
 * @todo write docs
 */
class ChatDisplayView : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * Default constructor
     */
    ChatDisplayView(std::shared_ptr<ChatDisplayViewModel> chatVM, QWidget* parent = nullptr);

    /**
     * Destructor
     */
    ~ChatDisplayView() noexcept;

signals:
    void onResized(const QSize& newSize);

private:
    std::shared_ptr<ChatDisplayViewModel> ChatViewModel;
    QScrollArea* ScrollArea;
    QWidget* containerWidget;
    QVBoxLayout* ChatEntryParent;
    int PrevScrollMax;

    void resizeEvent(QResizeEvent*);

    void InitUI();
    void AddChatRow(std::shared_ptr<ChatRowViewModel> chatRowVM);
    void RemoveChatRow(const int index);
    void HandleScrolling(int min, int max);
    void ClearChat();
    void HandlePreferencesUpdated(PreferenceUpdatedEvent updateEvt, PreferencesData newPrefs);
};

#endif // CHATDISPLAYVIEW_H
