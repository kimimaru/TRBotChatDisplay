/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <CustomTextFormViewModel.h>

CustomTextFormViewModel::CustomTextFormViewModel(std::shared_ptr<PreferencesViewModel> prefsVM, std::shared_ptr<ChatDisplayViewModel> chatVM)
{
    PrefsVM = prefsVM;
    ChatViewModel = chatVM;

    WhiteSpaceRegex = QRegularExpression(QRegularExpression::anchoredPattern("\\s*"));
}

void CustomTextFormViewModel::SetSentUsernameText(const QString& value)
{
    SentUsernameText = value;

    emit SentUsernameTextUpdated(value);
    emit CanSendMessageUpdated(CanSendMessage());
}

void CustomTextFormViewModel::SetSentMessageText(const QString& value)
{
    SentMessageText = value;

    emit SentMessageTextUpdated(value);
    emit CanSendMessageUpdated(CanSendMessage());
}

void CustomTextFormViewModel::SetSendMessageAsInput(const bool value)
{
    SendMessageAsInput = value;
}

void CustomTextFormViewModel::SetSendAsBotMessage(const bool value)
{
    SendAsBotMessage = value;
}

void CustomTextFormViewModel::SetClearSentMessageDetailsAfterSend(const bool value)
{
    ClearSentMessageDetailsAfterSend = value;
}

void CustomTextFormViewModel::SetSentMessageServiceName(const QString& value)
{
    SentMessageServiceName = value;

    emit SentMessageServiceNameUpdated(value);
}

void CustomTextFormViewModel::SendMessageToChat()
{
    if (CanSendMessage() == false)
    {
        return;
    }

    std::shared_ptr<ChatRowViewModel> chatVM = std::shared_ptr<ChatRowViewModel>(new ChatRowViewModel(
        PrefsVM, SentMessageServiceName, SentUsernameText, SentMessageText,
        QUuid::createUuid().toString(QUuid::StringFormat::WithoutBraces), SendMessageAsInput, SendAsBotMessage));

    ChatViewModel->AddChatEntry(chatVM);

    if (ClearSentMessageDetailsAfterSend == true)
    {
        SetSentUsernameText("");
        SetSentMessageText("");
    }
}

bool CustomTextFormViewModel::CanSendMessage() const
{
    return IsWhiteSpace(SentUsernameText) == false && IsWhiteSpace(SentMessageText) == false;
}

bool CustomTextFormViewModel::IsWhiteSpace(const QString& str) const
{
    auto match = WhiteSpaceRegex.match(str);

    return match.hasMatch();
}
