/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WEBSOCKETCONNECTIONVIEW_H
#define WEBSOCKETCONNECTIONVIEW_H

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <WebSocketConnectionViewModel.h>

class WebSocketConnectionView : public QWidget
{
    Q_OBJECT

public:
    WebSocketConnectionView(std::shared_ptr<WebSocketConnectionViewModel> webSocketVM);

private:
    std::shared_ptr<WebSocketConnectionViewModel> WebSocketVM;
    QLineEdit* DefaultWSURIBox;
    QPushButton* WSButton;

    void InitUI();
    void HandleWebSocketStateChange(QAbstractSocket::SocketState state);
};

#endif // WEBSOCKETCONNECTIONVIEW_H
