/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <ChatRowViewModel.h>
#include <PreferencesViewModel.h>

ChatRowViewModel::ChatRowViewModel(std::shared_ptr<PreferencesViewModel> prefsVM, const QString& serviceName, const QString& username, const QString& messageText,
    const QString& messageEventID, bool isInputMsg, bool isBotMsg)
{
    PrefsVM = prefsVM;

    ServiceName = serviceName;
    Username = username;
    MessageText = messageText;
    MessageEventID = messageEventID;
    isBotMessage = isBotMsg;

    SetIsInputMessage(isInputMsg);
}

bool ChatRowViewModel::GetIsInputMessage() const
{
    return isInputMessage;
}

bool ChatRowViewModel::GetIsBotMessage() const
{
    return isBotMessage;
}

void ChatRowViewModel::SetIsInputMessage(const bool value)
{
    if (isInputMessage != value)
    {
        isInputMessage = value;

        emit isInputMessageChanged(value);
    }
}

bool ChatRowViewModel::GetSeparatorVisible() const
{
    return isSeparatorVisible;
}

void ChatRowViewModel::SetSeparatorVisible(const bool value)
{
    if (isSeparatorVisible != value)
    {
        isSeparatorVisible = value;

        emit isSeparatorVisibleChanged(value);
    }
}

std::shared_ptr<PreferencesViewModel> ChatRowViewModel::GetPrefsVM()
{
    return PrefsVM;
}
