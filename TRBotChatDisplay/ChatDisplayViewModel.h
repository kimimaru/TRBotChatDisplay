/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHATDISPLAYVIEWMODEL_H
#define CHATDISPLAYVIEWMODEL_H

#include <ChatRowViewModel.h>
#include <PreferencesViewModel.h>
#include <WebSocketConnectionViewModel.h>

/**
 * @todo write docs
 */
class ChatDisplayViewModel : public QObject
{
    Q_OBJECT

public slots:
    void AddChatEntry(std::shared_ptr<ChatRowViewModel> chatRowVM);
    void RemoveChatEntry(const int index);
    void RemoveChatEntries(const int count);
    void ClearChat();

public:
    const int DefaultMaxChatEntries = 50;

    /**
     * Default constructor
     */
    ChatDisplayViewModel(std::shared_ptr<PreferencesViewModel> prefsVM, std::shared_ptr<WebSocketConnectionViewModel> webSocketVM, const QSize& minimumSize);

    /**
     * Destructor
     */
    ~ChatDisplayViewModel();

    QMargins GetChatPadding();
    void SetChatPadding(QMargins padding);

    QColor GetSeparatorColor();
    void SetSeparatorColor(QColor color);

    int GetSeparatorHeight();
    void SetSeparatorHeight(int height);

    QSize GetMinimumChatSize();

    QList<std::shared_ptr<ChatRowViewModel>> GetChatEntries();

    std::shared_ptr<PreferencesViewModel> GetPrefsVM();

signals:
    void onChatRowAdded(std::shared_ptr<ChatRowViewModel> chatRowVM);
    void onChatRowRemoved(const int index);
    void onChatCleared();

private:
    std::shared_ptr<PreferencesViewModel> PrefsVM;
    std::shared_ptr<WebSocketConnectionViewModel> WebSocketVM;

    QMargins chatPadding = QMargins();
    QColor separatorColor = QColorConstants::Gray;
    int separatorHeight = 1;
    QSize minimumChatSize = QSize();
    int maxChatEntries = DefaultMaxChatEntries;

    QList<std::shared_ptr<ChatRowViewModel>> chatEntries;

    void HandleWebSocketMessage(const QString& message);
    void HandlePreferencesUpdated(PreferenceUpdatedEvent updateEvt, PreferencesData newPrefs);
};

#endif // CHATDISPLAYVIEWMODEL_H
