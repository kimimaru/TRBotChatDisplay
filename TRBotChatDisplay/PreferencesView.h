/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PREFERENCESVIEW_H
#define PREFERENCESVIEW_H

#include <QObject>
#include <QComboBox>
#include <QWidget>
#include <QDialog>
#include <QScrollArea>
#include <QLabel>
#include <QVBoxLayout>
#include <QSpinBox>
#include <QCheckBox>
#include <QGroupBox>
#include <PreferencesViewModel.h>
#include <ColorPicker.h>
#include <FontPicker.h>

/**
 * @todo write docs
 */
class PreferencesView : public QWidget
{
    Q_OBJECT

public:
    /**
     * Default constructor
     */
    PreferencesView(std::shared_ptr<PreferencesViewModel> prefsVM, QWidget* parent = nullptr);

    /**
     * Destructor
     */
    ~PreferencesView() noexcept;

private:
    std::shared_ptr<PreferencesViewModel> PrefsViewModel;

    QCheckBox* ConnectWebSocketOnPrefsLoadBox;
    QLineEdit* WebSocketURILineEdit;
    ColorPicker* UserNameTextColorPicker;
    ColorPicker* MessageTextColorPicker;
    ColorPicker* InputTextColorPicker;
    FontPicker* ChatFontPicker;
    ColorPicker* ChatBGColorPicker;
    QSpinBox* ChatBoxWidthSpinBox;
    QSpinBox* ChatBoxHeightSpinBox;
    QComboBox* ChatBoxScrollBarBox;
    QSpinBox* ServiceIconSizeSpinBox;
    QSpinBox* ChatRowSpacingSpinBox;
    QSpinBox* MaxChatEntriesSpinBox;
    QCheckBox* ShowServiceIconBox;
    QCheckBox* ShowBotMessagesBox;
    QGroupBox* MessageToggleGroupBox;
    QCheckBox* TerminalMessageCheckbox;
    QCheckBox* TwitchMessageCheckbox;
    QCheckBox* WebSocketMessageCheckbox;
    QCheckBox* IRCMessageCheckbox;
    QCheckBox* XMPPMessageCheckbox;
    QCheckBox* MatrixMessageCheckbox;

    void InitUI();
    void HandlePreferencesUpdated(PreferenceUpdatedEvent updateEvt, PreferencesData newPrefs);
};

#endif // PREFERENCESVIEW_H
