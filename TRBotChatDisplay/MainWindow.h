/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenuBar>
#include <MainWindowViewModel.h>
#include <ChatDisplayView.h>
#include <CustomTextFormViewModel.h>
#include <CustomTextFormView.h>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(std::shared_ptr<MainWindowViewModel> mainWindowViewModel);
    ~MainWindow();

private:
    std::shared_ptr<MainWindowViewModel> mainWindowVM;

    ChatDisplayView* chatView;
    CustomTextFormView* customTextView;

    QMenu* fileMenu;
    QMenu* chatMenu;
    QMenu* helpMenu;

    QAction* loadPrefsAction;
    QAction* savePrefsAction;
    QAction* resetPrefsAction;

    QAction* openChatMenuAction;
    QAction* openCustomTextMenuAction;
    QAction* clearChatAction;

    QAction* openAboutMenuAction;
    QAction* openAboutQtMenuAction;

    void CreateActions();
    void CreateMenus();

    void OpenChat();
    void OpenCustomTextMenu();

    void ChatMenuClosed();
    void CustomTextMenuClosed();

    void OpenAboutMenu();
    void OpenAboutQtMenu();
};
#endif // MAINWINDOW_H
