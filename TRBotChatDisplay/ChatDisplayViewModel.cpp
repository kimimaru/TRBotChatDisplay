/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <ChatDisplayViewModel.h>
#include <QJsonObject>
#include <QJsonDocument>
#include <Constants.h>

ChatDisplayViewModel::ChatDisplayViewModel(std::shared_ptr<PreferencesViewModel> prefsVM, std::shared_ptr<WebSocketConnectionViewModel> webSocketVM, const QSize& minimumSize)
{
    PrefsVM = prefsVM;
    WebSocketVM = webSocketVM;
    minimumChatSize = minimumSize;

    connect(WebSocketVM.get(), &WebSocketConnectionViewModel::textMessageReceived, this, &ChatDisplayViewModel::HandleWebSocketMessage, Qt::QueuedConnection);
    connect(PrefsVM.get(), &PreferencesViewModel::onPreferencesUpdated, this, &ChatDisplayViewModel::HandlePreferencesUpdated);
}

ChatDisplayViewModel::~ChatDisplayViewModel()
{

}

QMargins ChatDisplayViewModel::GetChatPadding()
{
    return chatPadding;
}

void ChatDisplayViewModel::SetChatPadding(QMargins padding)
{
    chatPadding = padding;
}

QColor ChatDisplayViewModel::GetSeparatorColor()
{
    return separatorColor;
}

void ChatDisplayViewModel::SetSeparatorColor(QColor color)
{
    separatorColor = color;
}

int ChatDisplayViewModel::GetSeparatorHeight()
{
    return separatorHeight;
}

void ChatDisplayViewModel::SetSeparatorHeight(int height)
{
    separatorHeight = height;
}

QSize ChatDisplayViewModel::GetMinimumChatSize()
{
    return minimumChatSize;
}

QList<std::shared_ptr<ChatRowViewModel>> ChatDisplayViewModel::GetChatEntries()
{
    return chatEntries;
}

std::shared_ptr<PreferencesViewModel> ChatDisplayViewModel::GetPrefsVM()
{
    return PrefsVM;
}

void ChatDisplayViewModel::AddChatEntry(std::shared_ptr<ChatRowViewModel> chatRowVM)
{
    //Remove extra chat rows
    RemoveChatEntries((chatEntries.size() + 1) - PrefsVM->GetMaxChatEntries());

    if (chatEntries.size() > 0)
    {
        chatEntries[chatEntries.size() - 1]->SetSeparatorVisible(true);
    }

    chatRowVM->SetSeparatorVisible(false);

    chatEntries.append(chatRowVM);
    emit onChatRowAdded(chatRowVM);
}

void ChatDisplayViewModel::RemoveChatEntry(const int index)
{
    chatEntries.remove(index);
    emit onChatRowRemoved(index);
}

void ChatDisplayViewModel::RemoveChatEntries(const int count)
{
    for (int i = 0; i < count; i++)
    {
        RemoveChatEntry(0);
    }
}

void ChatDisplayViewModel::ClearChat()
{
    chatEntries.clear();
    emit onChatCleared();
}

void ChatDisplayViewModel::HandleWebSocketMessage(const QString& message)
{
    if (message.isEmpty() == true)
    {
        return;
    }

    QJsonDocument doc = QJsonDocument::fromJson(message.toUtf8());
    QJsonObject jsonObj = doc.object();

    const QString eventTypeFieldStr = "eventType";
    const QString eventDataFieldStr = "eventData";
    const QString serviceNameFieldStr = "serviceName";

    //No EventType, so this probably isn't connected to TRBot
    if (jsonObj.contains(eventTypeFieldStr) == false)
    {
        return;
    }

    if (jsonObj.contains(eventDataFieldStr) == false)
    {
        return;
    }

    QJsonObject eventJson = jsonObj[eventDataFieldStr].toObject();

    if (eventJson.contains(serviceNameFieldStr) == false)
    {
        return;
    }

    const QString userMessageFieldStr = "userMessage";
    const QString usernameFieldStr = "username";
    const QString messageFieldStr = "message";
    const QString eventIdFieldStr = "eventId";

    QString serviceName = eventJson[serviceNameFieldStr].toString();
    QString userName = "";
    QString messageStr = "";
    bool isInputMessage = false;
    bool isBotMessage = false;
    QString messageEventID = "";

    QString eventType = jsonObj[eventTypeFieldStr].toString();

    if (eventType == Constants::MessageEventName)
    {
        if (eventJson.contains(userMessageFieldStr) == false)
        {
            return;
        }

        QJsonObject usrMsgJson = eventJson[userMessageFieldStr].toObject();

        userName = usrMsgJson.contains(usernameFieldStr) ? usrMsgJson[usernameFieldStr].toString() : "Error";
        messageStr = usrMsgJson.contains(messageFieldStr) ? usrMsgJson[messageFieldStr].toString() : "Error";

        messageEventID = eventJson.contains(eventIdFieldStr) ? eventJson[eventIdFieldStr].toString() : "";
    }
    else if (eventType == Constants::InputEventName)
    {
        if (eventJson.contains(userMessageFieldStr) == false)
        {
            return;
        }

        const QString messageEventIdFieldStr = "messageEventId";

        QJsonObject usrMsgJson = eventJson[userMessageFieldStr].toObject();

        userName = usrMsgJson.contains(usernameFieldStr) ? usrMsgJson[usernameFieldStr].toString() : "Error";
        messageStr = usrMsgJson.contains(messageFieldStr) ? usrMsgJson[messageFieldStr].toString() : "Error";

        messageEventID = eventJson.contains(eventIdFieldStr) ? eventJson[eventIdFieldStr].toString() : "";

        QString associatedEventID = eventJson.contains(messageEventIdFieldStr) ? eventJson[messageEventIdFieldStr].toString() : "";

        if (associatedEventID.isEmpty() == false)
        {
            //Find a matching event ID for a previous message on a valid input and tell it it's an input instead of adding a new entry
            //Check the last 10 entries
            for (int i = chatEntries.size() - 1, index = 0; i >= 0 && index < 10; i--, index++)
            {
                auto chatVM = chatEntries[i];

                if (chatVM->MessageEventID == associatedEventID)
                {
                    chatVM->SetIsInputMessage(true);

                    return;
                }
            }
        }
    }
    else if (eventType == Constants::BotMessageEventName)
    {
        if (eventJson.contains(userMessageFieldStr) == false)
        {
            return;
        }

        QJsonObject usrMsgJson = eventJson[userMessageFieldStr].toObject();

        userName = usrMsgJson.contains(usernameFieldStr) ? usrMsgJson[usernameFieldStr].toString() : "Error";
        messageStr = usrMsgJson.contains(messageFieldStr) ? usrMsgJson[messageFieldStr].toString() : "Error";

        messageEventID = eventJson.contains(eventIdFieldStr) ? eventJson[eventIdFieldStr].toString() : "";

        isBotMessage = true;
    }
    else
    {
        return;
    }

    std::shared_ptr<ChatRowViewModel> chatRowVM = std::shared_ptr<ChatRowViewModel>(new ChatRowViewModel(PrefsVM, serviceName, userName, messageStr, messageEventID, isInputMessage, isBotMessage));

    AddChatEntry(chatRowVM);
}

void ChatDisplayViewModel::HandlePreferencesUpdated(PreferenceUpdatedEvent updateEvt, PreferencesData newPrefs)
{
    switch (updateEvt)
    {
        case PreferenceUpdatedEvent::MaxChatEntries:
            RemoveChatEntries(chatEntries.size() - PrefsVM->GetMaxChatEntries());
            break;
        default:
            break;
    }
}
