/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PREFERENCESVIEWMODEL_H
#define PREFERENCESVIEWMODEL_H

#include <PreferencesData.h>
#include <PreferenceUpdatedEvent.h>

class PreferencesViewModel : public QObject
{
    Q_OBJECT

public:
    PreferencesViewModel();

    QString SavePreferencesAsString();

public slots:

    bool GetConnectWebSocketOnLoad() const;
    void SetConnectWebSocketOnLoad(bool connectWSOnLoad);

    QString GetWebSocketDefaultURI() const;
    void SetWebSocketDefaultURI(const QString& defaultWSURI);

    QColor GetUsernameTextColor() const;
    void SetUsernameTextColor(const QColor& textColor);

    QColor GetMessageTextColor() const;
    void SetMessageTextColor(const QColor& textColor);

    QColor GetInputTextColor() const;
    void SetInputTextColor(const QColor& textColor);

    QFont GetChatFont() const;
    void SetChatFont(const QFont& chatFont);

    QColor GetChatBoxBackgroundColor() const;
    void SetChatBoxBackgroundColor(const QColor& chatBoxBGColor);

    QSize GetChatBoxSize() const;
    void SetChatBoxSize(const QSize& chatBoxSize);
    void SetChatBoxWidth(const int& chatBoxWidth);
    void SetChatBoxHeight(const int& chatBoxHeight);

    Qt::ScrollBarPolicy GetChatBoxScrollPolicy() const;
    void SetChatBoxScrollBarPolicy(const Qt::ScrollBarPolicy& scrollBarPolicy);
    void SetChatBoxScrollBarPolicy(int scrollBarPolicy);

    QSize GetServiceIconSize() const;
    void SetServiceIconSize(const QSize& serviceIconSize);
    void SetServiceIconSize(const int serviceIconSize);

    int GetChatRowSpacing() const;
    void SetChatRowSpacing(int rowSpacing);

    int GetMaxChatEntries() const;
    void SetMaxChatEntries(int maxEntries);

    bool GetShowBotMessages() const;
    void SetShowBotMessages(bool showBotMsgs);

    bool GetShowServiceIcons() const;
    void SetShowServiceIcons(bool showIcons);

    bool GetShowTerminalMessages() const;
    void SetShowTerminalMessages(bool showMsgs);

    bool GetShowTwitchMessages() const;
    void SetShowTwitchMessages(bool showMsgs);

    bool GetShowWebSocketMessages() const;
    void SetShowWebSocketMessages(bool showMsgs);

    bool GetShowIRCMessages() const;
    void SetShowIRCMessages(bool showMsgs);

    bool GetShowXMPPMessages() const;
    void SetShowXMPPMessages(bool showMsgs);

    bool GetShowMatrixMessages() const;
    void SetShowMatrixMessages(bool showMsg);

    void ResetPreferences();

    bool ValidatePreferencesJsonFromString(QByteArray preferencesJson);
    void LoadPreferencesFromString(QByteArray preferencesJson);

signals:
    void onPreferencesUpdated(PreferenceUpdatedEvent updateEvt, PreferencesData newPrefs);

private:
    PreferencesData preferencesData;

    void LoadPreferencesFromObject(PreferencesData newPrefs);
    QString ConvertQSizeToQString(const QSize& size);
    QSize ConvertQStringToQSize(const QString& sizeStr);
    QColor ConvertQStringToQColor(const QString& colorStr);

    const QString ConnectWebSocketLoadOnPrefs_Name = "ConnectWebSocketOnLoadPrefs";
    const QString DefaultWebSocketURI_Name = "DefaultWebSocketURI";

    const QString UsernameTextColor_Name = "UsernameTextColor";
    const QString MessageTextColor_Name = "MessageTextColor";
    const QString InputTextColor_Name = "InputTextColor";

    const QString ChatFontFamilyName_Name = "ChatFontFamilyName";
    const QString ChatFontSize_Name = "ChatFontSize";
    const QString ChatBoxBackgroundColor_Name = "ChatBoxBackgroundColor";
    const QString ChatBoxSize_Name = "ChatBoxSize";
    const QString ChatBoxScrollBarPolicy_Name = "ChatBoxScrollBar";
    const QString ServiceIconSize_Name = "ServiceIconSize";
    const QString ChatRowSpacing_Name = "ChatRowSpacing";
    const QString MaxChatEntries_Name = "MaxChatEntries";

    const QString ShowBotMessages_Name = "ShowBotMessages";
    const QString ShowServiceIcons_Name = "ShowServiceIcons";

    const QString ShowTerminalMessages_Name = "ShowTerminalMessages";
    const QString ShowTwitchMessages_Name = "ShowTwitchMessages";
    const QString ShowWebSocketMessages_Name = "ShowWebSocketMessages";
    const QString ShowIRCMessages_Name = "ShowIRCMessages";
    const QString ShowXMPPMessages_Name = "ShowXMPPMessages";
    const QString ShowMatrixMessages_Name = "ShowMatrixMessages";
};

#endif // PREFERENCESVIEWMODEL_H
