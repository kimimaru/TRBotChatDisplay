/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QMenuBar>
#include <MainWindow.h>
#include <ChatDisplayView.h>
#include <PreferencesView.h>
#include <WebSocketConnectionView.h>
#include <QIcon>
#include <QStyle>
#include <QMessageBox>
//#include <iostream>

/*void PrintDebug(PreferenceUpdatedEvent updateEvt, PreferencesData newPrefs)
{
    std::cout << "Update event: " << updateEvt << std::endl;
}*/

MainWindow::MainWindow(std::shared_ptr<MainWindowViewModel> mainWindowViewModel)
{
    mainWindowVM = mainWindowViewModel;
    setMinimumSize(mainWindowViewModel->MinimumSize);
    setWindowTitle("TRBot Chat Display");

    //connect(mainWindowVM->PrefsViewModel.get(), &PreferencesViewModel::onPreferencesUpdated, mainWindowVM->PrefsViewModel.get(), PrintDebug);

    CreateActions();
    CreateMenus();

    openChatMenuAction->trigger();
}

MainWindow::~MainWindow()
{

}

void MainWindow::CreateActions()
{
    loadPrefsAction = new QAction(style()->standardIcon(QStyle::StandardPixmap::SP_DialogOpenButton), tr("Load preferences..."), this);
    loadPrefsAction->setShortcuts(QKeySequence::Open);
    loadPrefsAction->setStatusTip(tr("Load preferences from a file."));
    connect(loadPrefsAction, &QAction::triggered, mainWindowVM.get(), &MainWindowViewModel::LoadPreferences);

    savePrefsAction = new QAction(style()->standardIcon(QStyle::StandardPixmap::SP_DialogSaveButton), tr("Save preferences..."), this);
    savePrefsAction->setShortcuts(QKeySequence::SaveAs);
    savePrefsAction->setStatusTip(tr("Save preferences to a file."));
    connect(savePrefsAction, &QAction::triggered, mainWindowVM.get(), &MainWindowViewModel::SavePreferences);

    resetPrefsAction = new QAction(style()->standardIcon(QStyle::StandardPixmap::SP_RestoreDefaultsButton), tr("Reset to defaults"), this);
    resetPrefsAction->setStatusTip(tr("Reset preferences to their default values."));
    connect(resetPrefsAction, &QAction::triggered, mainWindowVM.get(), &MainWindowViewModel::ResetPreferencesMenuOption);

    openChatMenuAction = new QAction(tr("Open chat"), this);
    openChatMenuAction->setStatusTip(tr("Opens the chatbox in a separate window."));
    connect(openChatMenuAction, &QAction::triggered, this, &MainWindow::OpenChat);

    openCustomTextMenuAction = new QAction(tr("Display custom text..."), this);
    openCustomTextMenuAction->setStatusTip(tr("Opens a menu that lets you input custom text to display in chat."));
    connect(openCustomTextMenuAction, &QAction::triggered, this, &MainWindow::OpenCustomTextMenu);

    clearChatAction = new QAction(tr("Clear chat"), this);
    clearChatAction->setStatusTip(tr("Clears all entries in the chatbox."));
    connect(clearChatAction, &QAction::triggered, mainWindowVM.get(), &MainWindowViewModel::ClearChatMenuOption);

    openAboutMenuAction = new QAction(tr("About"), this);
    openAboutMenuAction->setStatusTip(tr("Shows information about the application."));
    connect(openAboutMenuAction, &QAction::triggered, this, &MainWindow::OpenAboutMenu);

    openAboutQtMenuAction = new QAction(tr("About Qt"), this);
    openAboutQtMenuAction->setStatusTip(tr("Shows information about the Qt version used by this application."));
    connect(openAboutQtMenuAction, &QAction::triggered, this, &MainWindow::OpenAboutQtMenu);
}

void MainWindow::CreateMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(loadPrefsAction);
    fileMenu->addAction(savePrefsAction);
    fileMenu->addAction(resetPrefsAction);

    chatMenu = menuBar()->addMenu(tr("&Chat"));
    chatMenu->addAction(openChatMenuAction);
    chatMenu->addAction(openCustomTextMenuAction);
    chatMenu->addAction(clearChatAction);

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(openAboutMenuAction);
    helpMenu->addAction(openAboutQtMenuAction);

    QWidget* centralWidget = new QWidget();
    QVBoxLayout* verticalLayout = new QVBoxLayout(centralWidget);
    verticalLayout->setSpacing(0);

    verticalLayout->addWidget(new WebSocketConnectionView(mainWindowVM->WebSocketViewModel));
    verticalLayout->addWidget(new PreferencesView(mainWindowVM->PrefsViewModel));

    this->setCentralWidget(centralWidget);
}

void MainWindow::OpenChat()
{
    chatView = new ChatDisplayView(mainWindowVM->ChatViewModel, this);
    chatView->show();

    connect(chatView, &ChatDisplayView::destroyed, this, &MainWindow::ChatMenuClosed);

    openChatMenuAction->setEnabled(false);
}

void MainWindow::OpenCustomTextMenu()
{
    std::shared_ptr<CustomTextFormViewModel> vm = std::shared_ptr<CustomTextFormViewModel>(new CustomTextFormViewModel(mainWindowVM->PrefsViewModel, mainWindowVM->ChatViewModel));

    customTextView = new CustomTextFormView(vm, this);
    customTextView->show();

    connect(customTextView, &CustomTextFormView::destroyed, this, &MainWindow::CustomTextMenuClosed);

    openCustomTextMenuAction->setEnabled(false);
}

void MainWindow::ChatMenuClosed()
{
    openChatMenuAction->setEnabled(true);
}

void MainWindow::CustomTextMenuClosed()
{
    openCustomTextMenuAction->setEnabled(true);
}

void MainWindow::OpenAboutMenu()
{
    QMessageBox::about(
        this,
        "About TRBot Chat Display",
        QString("TRBot Chat Display\nv%1.%2.%3\n\nA chat display for TRBot.\nhttps://codeberg.org/kimimaru/TRBotChatDisplay\n\nLicense: AGPLv3 or later\nhttps://www.gnu.org/licenses/agpl-3.0.en.html\n\nCopyright (C) 2023 Thomas \"Kimimaru\" Deeb").arg(VERSION_MAJOR).arg(VERSION_MINOR).arg(VERSION_BUILD));
}

void MainWindow::OpenAboutQtMenu()
{
    QMessageBox::aboutQt(this);
}
