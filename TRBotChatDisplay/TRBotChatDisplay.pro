QT       += core gui
QT       += websockets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17
CONFIG += release

#Application version
VERSION_MAJOR = 0
VERSION_MINOR = 3
VERSION_BUILD = 0

VERSION = $${VERSION_MAJOR}.$${VERSION_MINOR}.$${VERSION_BUILD}

DEFINES += VERSION_MAJOR=$$VERSION_MAJOR
DEFINES += VERSION_MINOR=$$VERSION_MINOR
DEFINES += VERSION_BUILD=$$VERSION_BUILD

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    PreferenceUpdatedEvent.cpp \
    PreferencesData.cpp \
    PreferencesViewModel.cpp \
    ChatDisplayViewModel.cpp \
    ChatRowViewModel.cpp \
    MainWindowViewModel.cpp \
    main.cpp \
    MainWindow.cpp \
    ChatDisplayView.cpp \
    CustomTextFormViewModel.cpp \
    CustomTextFormView.cpp \
    ChatRowView.cpp \
    PreferencesView.cpp \
    ColorPicker.cpp \
    FontPicker.cpp \
    WebSocketConnectionViewModel.cpp \
    WebSocketConnectionView.cpp \
    Constants.cpp

HEADERS += \
    PreferenceUpdatedEvent.h \
    PreferencesData.h \
    PreferencesViewModel.h \
    ChatDisplayViewModel.h \
    ChatRowViewModel.h \
    MainWindowViewModel.h \
    MainWindow.h \
    ChatDisplayView.h \
    CustomTextFormViewModel.h \
    CustomTextFormView.h \
    ChatRowView.h \
    PreferencesView.h \
    ColorPicker.h \
    FontPicker.h \
    WebSocketConnectionViewModel.h \
    WebSocketConnectionView.h \
    Constants.h

RESOURCES=TRBotChatDisplay.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
