/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHATROWVIEWMODEL_H
#define CHATROWVIEWMODEL_H

#include <PreferencesViewModel.h>

class ChatRowViewModel : public QObject
{
    Q_OBJECT

public:
    QString ServiceName = "";
    QString Username = "";
    QString MessageText = "";
    QString MessageEventID = "";

    ChatRowViewModel(std::shared_ptr<PreferencesViewModel> prefsVM, const QString& serviceName, const QString& username, const QString& messageText, const QString& messageEventID, bool isInputMsg, bool isBotMsg);

    bool GetIsInputMessage() const;
    bool GetIsBotMessage() const;
    bool GetSeparatorVisible() const;

    std::shared_ptr<PreferencesViewModel> GetPrefsVM();

public slots:
    void SetIsInputMessage(const bool);
    void SetSeparatorVisible(const bool);

signals:
    void isInputMessageChanged(const bool);
    void isSeparatorVisibleChanged(const bool);

private:
    std::shared_ptr<PreferencesViewModel> PrefsVM;

    bool isInputMessage = false;
    bool isBotMessage = false;
    bool isSeparatorVisible = false;
};

#endif // CHATROWVIEWMODEL_H
