/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QObject>
#include <QWidget>
#include <QDialog>
#include <Qt>
#include <QScrollArea>
#include <QLabel>
#include <QMainWindow>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QComboBox>
#include <QCheckBox>
#include <Constants.h>
#include <CustomTextFormView.h>
#include <QMetaEnum>

CustomTextFormView::CustomTextFormView(std::shared_ptr<CustomTextFormViewModel> customTextFormVM, QWidget* parent)
    : QMainWindow(parent)
{
    CustomTextFormVM = customTextFormVM;
    this->setWindowTitle("TRBotChatDisplay - Custom Text Window");

    InitUI();
}

CustomTextFormView::~CustomTextFormView()
{

}

void CustomTextFormView::InitUI()
{
    this->setAttribute(Qt::WidgetAttribute::WA_DeleteOnClose);

    QWidget* centralWidget = new QWidget();
    this->setCentralWidget(centralWidget);

    QVBoxLayout* verticalLayout = new QVBoxLayout(centralWidget);
    verticalLayout->setSizeConstraint(QVBoxLayout::SizeConstraint::SetMaximumSize);

    QHBoxLayout* horizontalLabelLayout = new QHBoxLayout();
    horizontalLabelLayout->addWidget(new QLabel(tr("Use this menu to post messages to the chat box. This is only visual and doesn't actually send the messages anywhere.")));

    QLineEdit* userNameField = new QLineEdit();
    QLineEdit* messageField = new QLineEdit();

    QComboBox* serviceComboBox = new QComboBox();
    QMetaEnum metaEnum = QMetaEnum::fromType<Constants::ClientServiceNames>();
    for (int i = 0; i < metaEnum.keyCount(); i++)
    {
        serviceComboBox->addItem(metaEnum.key(i));
    }

    QHBoxLayout* horizontalInputLayout = new QHBoxLayout();
    horizontalInputLayout->addWidget(new QLabel(tr("Username")));
    horizontalInputLayout->addWidget(userNameField);
    horizontalInputLayout->addWidget(new QLabel(tr("Message")));
    horizontalInputLayout->addWidget(messageField);
    horizontalInputLayout->addWidget(new QLabel(tr("Service to display")));
    horizontalInputLayout->addWidget(serviceComboBox);

    QCheckBox* sendAsInputBox = new QCheckBox(tr("Send as input message"));
    QCheckBox* sendAsBotMsgBox = new QCheckBox(tr("Send as bot message"));
    QCheckBox* clearAfterSendBox = new QCheckBox(tr("Clear username and message after send"));

    QHBoxLayout* horizontalCheckboxLayout = new QHBoxLayout();
    horizontalCheckboxLayout->addWidget(sendAsInputBox);
    horizontalCheckboxLayout->addWidget(sendAsBotMsgBox);
    horizontalCheckboxLayout->addWidget(clearAfterSendBox);

    QPushButton* postMsgBtn = new QPushButton(tr("Post message"));
    postMsgBtn->setEnabled(false);

    verticalLayout->addLayout(horizontalLabelLayout);
    verticalLayout->addLayout(horizontalInputLayout);
    verticalLayout->addLayout(horizontalCheckboxLayout);
    verticalLayout->addWidget(postMsgBtn);

    connect(CustomTextFormVM.get(), &CustomTextFormViewModel::SentUsernameTextUpdated, userNameField, &QLineEdit::setText);
    connect(userNameField, &QLineEdit::textChanged, CustomTextFormVM.get(), &CustomTextFormViewModel::SetSentUsernameText);

    connect(CustomTextFormVM.get(), &CustomTextFormViewModel::SentMessageTextUpdated, messageField, &QLineEdit::setText);
    connect(messageField, &QLineEdit::textChanged, CustomTextFormVM.get(), &CustomTextFormViewModel::SetSentMessageText);

    connect(clearAfterSendBox, &QCheckBox::toggled, CustomTextFormVM.get(), &CustomTextFormViewModel::SetClearSentMessageDetailsAfterSend);
    connect(sendAsInputBox, &QCheckBox::toggled, CustomTextFormVM.get(), &CustomTextFormViewModel::SetSendMessageAsInput);
    connect(sendAsBotMsgBox, &QCheckBox::toggled, CustomTextFormVM.get(), &CustomTextFormViewModel::SetSendAsBotMessage);

    connect(CustomTextFormVM.get(), &CustomTextFormViewModel::CanSendMessageUpdated, postMsgBtn, &QPushButton::setEnabled);
    connect(postMsgBtn, &QPushButton::clicked, CustomTextFormVM.get(), &CustomTextFormViewModel::SendMessageToChat, Qt::ConnectionType::QueuedConnection);

    connect(CustomTextFormVM.get(), &CustomTextFormViewModel::SentMessageServiceNameUpdated, serviceComboBox, &QComboBox::setCurrentText);
    connect(serviceComboBox, &QComboBox::currentTextChanged, CustomTextFormVM.get(), &CustomTextFormViewModel::SetSentMessageServiceName);
}
