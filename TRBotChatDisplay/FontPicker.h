/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FONTPICKER_H
#define FONTPICKER_H

#include <QObject>
#include <QWidget>
#include <QFontDialog>
#include <QPushButton>

class FontPicker : public QPushButton
{
    Q_OBJECT

public:
    FontPicker(const QFont &initial, QWidget *parent = nullptr);
    ~FontPicker();

public slots:
    void SetFont(const QFont &font);

signals:
    void currentFontChanged(const QFont &font);

private:
    QFont CurrentFont;

    void OnFontButtonPressed();
    void UpdateButtonFont();
};

#endif // FONTPICKER_H
