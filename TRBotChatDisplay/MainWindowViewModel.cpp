/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <MainWindowViewModel.h>
#include <ChatDisplayView.h>
//#include <iostream>

MainWindowViewModel::MainWindowViewModel(std::shared_ptr<PreferencesViewModel> prefsVM, std::shared_ptr<ChatDisplayViewModel> chatVM, std::shared_ptr<WebSocketConnectionViewModel> webSocketVM, QSize minimumSize)
{
    PrefsViewModel = prefsVM;
    ChatViewModel = chatVM;
    WebSocketViewModel = webSocketVM;
    MinimumSize = minimumSize;
}

MainWindowViewModel::~MainWindowViewModel()
{

}

void MainWindowViewModel::LoadPreferences()
{
    QString fileName = QFileDialog::getOpenFileName(
        nullptr,
        tr("Load preferences file"),
        DefaultPrefsFileName,
        tr("JSON (*.json)"),
        nullptr,
        QFileDialog::Options());

    if (fileName.isEmpty() == true)
    {
        return;
    }

    //std::cout << "Load: " << fileName.toStdString() << std::endl;

    QFile prefsFile(fileName);

    if (prefsFile.open(QIODevice::ReadOnly | QIODevice::Text) == false)
    {
        return;
    }

    QTextStream in(&prefsFile);

    QString loadedPrefsJson = in.readAll();

    prefsFile.close();

    //std::cout << "Loaded JSON: " << loadedPrefsJson.toStdString() << std::endl;

    auto jsonArray = loadedPrefsJson.toUtf8();

    QString missingMember;

    if (PrefsViewModel->ValidatePreferencesJsonFromString(jsonArray) == false)
    {
        QMessageBox::critical(
            nullptr,
            tr("Invalid JSON File"),
            tr("The JSON file provided is not valid for this application.")
        );

        return;
    }

    PrefsViewModel->LoadPreferencesFromString(jsonArray);
}

void MainWindowViewModel::SavePreferences()
{
    QString fileName = QFileDialog::getSaveFileName(
        nullptr,
        tr("Save preferences file"),
        DefaultPrefsFileName,
        tr("JSON (*.json)"),
        nullptr,
        QFileDialog::Options());

    if (fileName.isEmpty() == true)
    {
        return;
    }

    QFile prefsFile(fileName);

    if (prefsFile.open(QIODevice::WriteOnly | QIODevice::Text) == false)
    {
        return;
    }

    QString jsonFile = PrefsViewModel->SavePreferencesAsString();

    QTextStream out(&prefsFile);

    out << jsonFile << "\n";

    prefsFile.close();
}

void MainWindowViewModel::ResetPreferencesMenuOption()
{
    int ret = QMessageBox::question(nullptr,
                tr("Reset Preferences"),
                tr("Reset preferences to defaults?"),
                QMessageBox::Yes, QMessageBox::No);

    switch (ret)
    {
        case QMessageBox::Yes:
            PrefsViewModel->ResetPreferences();
            break;
        default:
            break;
    }
}

void MainWindowViewModel::ClearChatMenuOption()
{
    int ret = QMessageBox::question(nullptr,
                tr("Clear chatbox"),
                tr("Clear all messages in the chatbox?"),
                QMessageBox::Yes, QMessageBox::No);

    switch (ret)
    {
        case QMessageBox::Yes:
            ChatViewModel->ClearChat();
            break;
        default:
            break;
    }
}
