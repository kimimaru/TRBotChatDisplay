/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <ChatDisplayView.h>
#include <ChatRowView.h>
#include <QSizePolicy>
#include <QVBoxLayout>
#include <QList>
#include <QAbstractSlider>
#include <QScrollBar>
#include <QResizeEvent>
#include <Constants.h>

ChatDisplayView::ChatDisplayView(std::shared_ptr<ChatDisplayViewModel> chatVM, QWidget* parent)
    : QMainWindow(parent)
{
    this->setAttribute(Qt::WidgetAttribute::WA_DeleteOnClose);
    this->setWindowTitle("TRBotChatDisplay - Chat Window");

    ChatViewModel = chatVM;

    InitUI();
}

ChatDisplayView::~ChatDisplayView() noexcept
{

}

void ChatDisplayView::resizeEvent(QResizeEvent* evt)
{
    emit onResized(evt->size());
}

void ChatDisplayView::InitUI()
{
    auto prefsVM = ChatViewModel->GetPrefsVM();

    this->setMinimumSize(ChatViewModel->GetMinimumChatSize());
    this->resize(prefsVM->GetChatBoxSize());

    ScrollArea = new QScrollArea();
    containerWidget = new QWidget();
    containerWidget->setObjectName("chatdisplayview");

    ChatEntryParent = new QVBoxLayout();
    ChatEntryParent->setObjectName("ChatEntryParent");
    ChatEntryParent->setSpacing(prefsVM->GetChatRowSpacing());
    ChatEntryParent->setAlignment(Qt::AlignmentFlag::AlignTop);

    containerWidget->setLayout(ChatEntryParent);
    containerWidget->setStyleSheet("QWidget#" + containerWidget->objectName() + " { background : " + prefsVM->GetChatBoxBackgroundColor().name(QColor::NameFormat::HexRgb) + "; }");

    ScrollArea->setFrameShape(QFrame::NoFrame);
    ScrollArea->setAlignment(Qt::AlignmentFlag::AlignCenter | Qt::AlignmentFlag::AlignTop);
    ScrollArea->setVerticalScrollBarPolicy(prefsVM->GetChatBoxScrollPolicy());
    ScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    ScrollArea->setWidgetResizable(true);

    ScrollArea->setWidget(containerWidget);

    this->setCentralWidget(ScrollArea);

    connect(ChatViewModel.get(), &ChatDisplayViewModel::onChatRowAdded, this, &ChatDisplayView::AddChatRow);
    connect(ChatViewModel.get(), &ChatDisplayViewModel::onChatRowRemoved, this, &ChatDisplayView::RemoveChatRow);
    connect(ChatViewModel.get(), &ChatDisplayViewModel::onChatCleared, this, &ChatDisplayView::ClearChat);

    auto list = ChatViewModel->GetChatEntries();

    PrevScrollMax = ScrollArea->verticalScrollBar()->maximum();

    for (int i = 0; i < list.count(); i++)
    {
        AddChatRow(list[i]);
    }

    QScrollBar* scrollbar = ScrollArea->verticalScrollBar();
    connect(scrollbar, &QAbstractSlider::rangeChanged, this, &ChatDisplayView::HandleScrolling);

    connect(prefsVM.get(), &PreferencesViewModel::onPreferencesUpdated, this, &ChatDisplayView::HandlePreferencesUpdated);
    connect(this, &ChatDisplayView::onResized, prefsVM.get(), &PreferencesViewModel::SetChatBoxSize);
}

void ChatDisplayView::AddChatRow(std::shared_ptr<ChatRowViewModel> chatRowVM)
{
    ChatRowView* chatRowView = new ChatRowView(chatRowVM);
    ChatEntryParent->addWidget(chatRowView);
}

void ChatDisplayView::RemoveChatRow(const int index)
{
    auto child = ChatEntryParent->takeAt(index);
    delete child->widget();
    delete child;
}

void ChatDisplayView::HandleScrolling(int min, int max)
{
    Q_UNUSED(min);

    QScrollBar* scroll = ScrollArea->verticalScrollBar();

    //Scroll
    if (scroll->value() >= PrevScrollMax)
    {
        PrevScrollMax = max;
        scroll->setValue(max);
    }
    //The maximum decreased, likely from chat being cleared
    else if (max < PrevScrollMax)
    {
        PrevScrollMax = max;
    }
}

void ChatDisplayView::ClearChat()
{
    QLayoutItem *child;
    while ((child = ChatEntryParent->takeAt(0)) != nullptr)
    {
        //Delete the widget
        delete child->widget();

        //Delete the layout item
        delete child;
    }
}

void ChatDisplayView::HandlePreferencesUpdated(PreferenceUpdatedEvent updateEvt, PreferencesData newPrefs)
{
    auto prefsVM = ChatViewModel->GetPrefsVM();

    switch (updateEvt)
    {
        case PreferenceUpdatedEvent::ChatBoxBGColor:
            containerWidget->setStyleSheet("QWidget#" + containerWidget->objectName() + " { background : " + prefsVM->GetChatBoxBackgroundColor().name(QColor::NameFormat::HexRgb) + "; }");
            break;
        case PreferenceUpdatedEvent::ChatBoxSize:
            this->resize(prefsVM->GetChatBoxSize());
            break;
        case PreferenceUpdatedEvent::ChatRowSpacing:
            ChatEntryParent->setSpacing(prefsVM->GetChatRowSpacing());
            break;
        case PreferenceUpdatedEvent::ChatBoxScrollBar:
            ScrollArea->setVerticalScrollBarPolicy(prefsVM->GetChatBoxScrollPolicy());
            break;
        default:
            break;
    }
}
