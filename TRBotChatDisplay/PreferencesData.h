/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PREFERENCESDATA_H
#define PREFERENCESDATA_H

#include <QSize>
#include <QColor>
#include <QFont>
#include <Constants.h>

class PreferencesData
{
public:
    PreferencesData();

    bool ConnectWebSocketOnLoadPrefs = false;
    QString DefaultWebSocketURI = "ws://127.0.0.1:4350/evt";

    QColor UsernameTextColor = QColor(135, 206, 235, 255);
    QColor MessageTextColor = QColorConstants::White;
    QColor InputTextColor = QColor(34, 139, 34, 255);

    QFont ChatFont = QFont();
    QColor ChatBoxBackgroundColor = QColorConstants::Black;
    QSize ChatBoxSize = QSize(600, 350);
    Qt::ScrollBarPolicy ChatBoxScrollBarPolicy = Qt::ScrollBarAsNeeded;
    QSize ServiceIconSize = Constants::DefaultServiceImageSize;
    int ChatRowSpacing = 3;
    int MaxChatEntries = 100;

    bool ShowBotMessages = true;
    bool ShowServiceIcons = true;

    bool ShowTerminalMessages = true;
    bool ShowTwitchMessages = true;
    bool ShowWebSocketMessages = true;
    bool ShowIRCMessages = true;
    bool ShowXMPPMessages = true;
    bool ShowMatrixMessages = true;
};

#endif // PREFERENCESDATA_H
