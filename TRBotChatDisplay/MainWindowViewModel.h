/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOWVIEWMODEL_H
#define MAINWINDOWVIEWMODEL_H

#include <PreferencesViewModel.h>
#include <ChatDisplayViewModel.h>
#include <WebSocketConnectionViewModel.h>

/**
 * @todo write docs
 */
class MainWindowViewModel : public QObject
{
    Q_OBJECT

public slots:
    void LoadPreferences();
    void SavePreferences();
    void ResetPreferencesMenuOption();
    void ClearChatMenuOption();

public:
    const QString DefaultPrefsFileName = "TRBotChatDisplayPrefs.json";

    std::shared_ptr<PreferencesViewModel> PrefsViewModel;
    std::shared_ptr<ChatDisplayViewModel> ChatViewModel;
    std::shared_ptr<WebSocketConnectionViewModel> WebSocketViewModel;

    QSize MinimumSize;
    QSize StartingChatSize;

    MainWindowViewModel(std::shared_ptr<PreferencesViewModel> prefsVM, std::shared_ptr<ChatDisplayViewModel> chatVM, std::shared_ptr<WebSocketConnectionViewModel> webSocketVM, QSize minimumSize);

    /**
     * Destructor
     */
    virtual ~MainWindowViewModel();
};

#endif // MAINWINDOWVIEWMODEL_H
