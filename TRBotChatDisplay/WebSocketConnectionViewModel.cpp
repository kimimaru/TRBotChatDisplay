/* Copyright (C) 2023 Thomas "Kimimaru" Deeb
 *
 * This file is part of TRBotChatDisplay.
 *
 * TRBotChatDisplay is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TRBotChatDisplay is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with TRBotChatDisplay.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <WebSocketConnectionViewModel.h>
#include <iostream>

WebSocketConnectionViewModel::WebSocketConnectionViewModel(std::shared_ptr<PreferencesViewModel> prefsVM)
{
    PrefsVM = prefsVM;

    SetCurrentWebSocketURI(PrefsVM->GetWebSocketDefaultURI());

    connect(&WebSocket, &QWebSocket::connected, this, &WebSocketConnectionViewModel::OnWebSocketConnected);
    connect(&WebSocket, &QWebSocket::disconnected, this, &WebSocketConnectionViewModel::OnWebSocketDisconnected);
    connect(&WebSocket, &QWebSocket::stateChanged, this, &WebSocketConnectionViewModel::OnWebSocketStateChanged);
    connect(&WebSocket, &QWebSocket::textMessageReceived, this, &WebSocketConnectionViewModel::OnWebSocketTextMessageReceived);

    connect(PrefsVM.get(), &PreferencesViewModel::onPreferencesUpdated, this, &WebSocketConnectionViewModel::HandlePreferencesUpdated);
}

QString WebSocketConnectionViewModel::GetCurrentWebSocketURI()
{
    return CurrentWebSocketURI;
}

void WebSocketConnectionViewModel::SetCurrentWebSocketURI(const QString& newURI)
{
    CurrentWebSocketURI = newURI;
}

void WebSocketConnectionViewModel::OnWebSocketConnected()
{
    std::cout << "Connected to WebSocket address: " << "\"" << CurrentWebSocketURI.toStdString() << "\"" << std::endl;
}

void WebSocketConnectionViewModel::OnWebSocketDisconnected()
{
    std::cout << "Disconnected from WebSocket" << std::endl;
}

void WebSocketConnectionViewModel::OnWebSocketStateChanged(QAbstractSocket::SocketState state)
{
    std::cout << "WebSocket state changed to: " << state << std::endl;

    emit stateChanged(state);
}

void WebSocketConnectionViewModel::OnWebSocketTextMessageReceived(const QString& message)
{
    std::cout << "Message received: " << message.toStdString() << std::endl;

    emit textMessageReceived(message);
}

void WebSocketConnectionViewModel::ToggleWebSocketConnection()
{
    if (CanWebSocketBeClosed() == true)
    {
        WebSocket.close();
        return;
    }

    WebSocket.open(CurrentWebSocketURI);
}

bool WebSocketConnectionViewModel::CanWebSocketBeClosed()
{
    return WebSocket.state() == QAbstractSocket::SocketState::ConnectedState || WebSocket.state() == QAbstractSocket::SocketState::ConnectingState;
}

void WebSocketConnectionViewModel::HandlePreferencesUpdated(PreferenceUpdatedEvent updateEvt, PreferencesData newPrefs)
{
    switch (updateEvt)
    {
        case PreferenceUpdatedEvent::LoadedFromFile:
            //Change the current URI only if the WebSocket isn't connected
            if (WebSocket.state() == QAbstractSocket::SocketState::UnconnectedState)
            {
                SetCurrentWebSocketURI(PrefsVM->GetWebSocketDefaultURI());
                emit WebSocketURILoaded(CurrentWebSocketURI);

                //If it should connect directly, open the connection now
                if (PrefsVM->GetConnectWebSocketOnLoad() == true)
                {
                    WebSocket.open(CurrentWebSocketURI);
                }
            }

            break;
        default:
            break;
    }
}
